package lt.vcs.nikolajus.coffeMaker;

import lt.vcs.nikolajus.foodToolsFactory.cup.CoffeeCup;

import java.util.*;

import static lt.vcs.nikolajus.AdditionalTools.*;


public class CoffeeMaker {


    public final int USE_LIMIT = 500;
    public final int MAX_NUMBER_OF_SMALL_COFFEE_CUPS = 250;
    public final int MAX_NUMBER_OF_BIG_COFFEE_CUPS = 250;
    public final int MIN_SMALL_CUP_CAPACITY = 160; // in milliliters
    public final int MAX_SMALL_CUP_CAPACITY = 250; // in milliliters
    public final int MIN_BIG_CUP_CAPACITY = 250; // in milliliters
    public final int MAX_BIG_CUP_CAPACITY = 500; // in milliliters
    private List<CoffeeCup> smallCups;
    private List<CoffeeCup> bigCups;
    private int useCounter;
    private Products products;
    private CoffeeTypes coffeeTypes;
    private Map<String, Boolean> availabilityToMakeSmallCoffeeCup;
    private Map<String, Boolean> availabilityToMakeBigCoffeeCup;


    public CoffeeMaker() {
        this(new Products());
    }

    public CoffeeMaker(Products products) {
        this.smallCups = new ArrayList<>();
        this.bigCups = new ArrayList<>();
        this.products = products;
        this.coffeeTypes = new CoffeeTypes();
        this.useCounter = 0;
        this.availabilityToMakeSmallCoffeeCup = new TreeMap<>();
        this.availabilityToMakeBigCoffeeCup = new TreeMap<>();
        for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
            this.availabilityToMakeSmallCoffeeCup.put(coffeeType.getKey(), false);
            this.availabilityToMakeBigCoffeeCup.put(coffeeType.getKey(), false);
        }
    }


    public void turnOnCoffeeMaker() {
        boolean callOfServiceWorker = false;
        //MENU
        do {
            List<String> coffeeTypeNames = new ArrayList<>();
            int i;

            for (Map.Entry<String, Map<String, Double>> coffeeType : coffeeTypes.getCoffeeTypes().entrySet()) {
                coffeeTypeNames.add(coffeeType.getKey());
            }

            System.out.println("\nKAVOS APARATO MENIU");
            System.out.println("\nPasirinkite viena is zemiau pateiktu gerimu, iveskite gerimo numeri ir paspauskite Enter:");
            for (i = 1; i <= coffeeTypeNames.size(); i++) {
                System.out.println(i + ". " + coffeeTypeNames.get(i - 1) +
                        (((this.availabilityToMakeSmallCoffeeCup.get(coffeeTypeNames.get(i - 1))) ||
                                (this.availabilityToMakeBigCoffeeCup.get(coffeeTypeNames.get(i - 1)))) ? "" : " (nepasiekiamas)"));
            }
            System.out.println("\n" + i + ". Jei norite paziureti esamu kavos aparato komponentu likuti, spauskite " + (i) + ";");
            System.out.println((i + 1) + ". Jei norite iskviesti serviso darbuotoja, spauskite " + (i + 1) + ";");
            System.out.println((i + 2) + ". Jei norite isjungti kavos aparata, spauskite " + (i + 2) + ", ir paspauskite Enter:");


            boolean noComponents = false;
            boolean lowComponents = false;
            //Check if there is all components
            if ((getNumberOfSmallCups() == 0) || (getNumberOfBigCups() == 0)) {
                noComponents = true;
            }
            if (!noComponents) {
                for (Map.Entry<String, Boolean> hasNoIngredient : this.products.getProductsIsOver().entrySet()) {
                    if (hasNoIngredient.getValue()) {
                        noComponents = true;
                        break;
                    }
                }
            }
            //Check if there is low content components
            if (((getNumberOfSmallCups() < (MAX_NUMBER_OF_SMALL_COFFEE_CUPS / 10)) & (getNumberOfSmallCups() != 0)) ||
                    ((getNumberOfBigCups() < (MAX_NUMBER_OF_BIG_COFFEE_CUPS / 10)) & (getNumberOfBigCups() != 0))) {
                lowComponents = true;
            }
            if (!lowComponents) {
                for (Map.Entry<String, Boolean> lowIngredient : this.products.getLowContentProducts().entrySet()) {
                    if ((lowIngredient.getValue()) && (!this.products.getProductsIsOver().get(lowIngredient.getKey()))) {
                        lowComponents = true;
                    }
                }
            }

            if (((USE_LIMIT - this.useCounter) < (USE_LIMIT / 10)) || noComponents || lowComponents) {
                System.out.println("\nPASTABA:");
            }
            if ((USE_LIMIT - this.useCounter) < (USE_LIMIT / 10)) {
                System.out.println("\nIsvalykite kavos aparata.");
            }
            if (noComponents) {
                String noComponentsNames = "";
                for (Map.Entry<String, Boolean> hasNoIngredient : this.products.getProductsIsOver().entrySet()) {
                    if (hasNoIngredient.getValue()) {
                        noComponentsNames = noComponentsNames.concat(hasNoIngredient.getKey() + ", ");
                    }
                }
                if (getNumberOfSmallCups() == 0) {
                    noComponentsNames = noComponentsNames.concat("mazu puoduku, ");
                }
                if (getNumberOfBigCups() == 0) {
                    noComponentsNames = noComponentsNames.concat("dideliu puoduku, ");
                }
                System.out.println("Kavos aparate nera komponentu: " + noComponentsNames.substring(0, noComponentsNames.lastIndexOf(",")) + ".");
            }

            if (lowComponents) {
                String lowComponentsNames = "";
                for (Map.Entry<String, Boolean> hasLowIngredient : this.products.getLowContentProducts().entrySet()) {
                    if (hasLowIngredient.getValue() && !(this.products.getProductsIsOver().get(hasLowIngredient.getKey()))) {
                        lowComponentsNames = lowComponentsNames.concat(hasLowIngredient.getKey() + ", ");
                    }
                }
                if ((getNumberOfSmallCups() < (MAX_NUMBER_OF_SMALL_COFFEE_CUPS / 10)) & (getNumberOfSmallCups() != 0)) {
                    lowComponentsNames = lowComponentsNames.concat("mazi puodukai, ");
                }
                if ((getNumberOfBigCups() < (MAX_NUMBER_OF_BIG_COFFEE_CUPS / 10)) & (getNumberOfBigCups() != 0)) {
                    lowComponentsNames = lowComponentsNames.concat("dideli puodukai, ");
                }
                System.out.println("Kavos aparate baiginejasi: " + lowComponentsNames.substring(0, lowComponentsNames.lastIndexOf(",")) + ".");
            }

            int choice = chooseItem("", (i + 2));
            if (choice <= coffeeTypeNames.size()) {
                String coffeeTypeName = coffeeTypeNames.get(choice - 1);
                int cupSizeId = chooseItem("\n1. Jei norite pasirinkti maza puoduka, spauskite 1" +
                        (((getNumberOfSmallCups() == 0) || (!this.availabilityToMakeSmallCoffeeCup.get(coffeeTypeName)))
                                ? " (nepasiekiamas)" : "") + "\n2. Jei norite pasirinkti dideli" +
                        " puoduka, spauskite 2" + (((getNumberOfBigCups() == 0) ||
                        (!this.availabilityToMakeBigCoffeeCup.get(coffeeTypeName))) ? " (nepasiekiamas)" : ""), 2);
                try {
                    makeCoffee(coffeeTypeNames.get(choice - 1), ((cupSizeId == 1) ? false : true),
                            chooseItem("\nPasirinkite cukraus kieki nuo 0 iki 5:", 0, 5));
                } catch (CoffeeMakingException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                switch (choice - coffeeTypeNames.size()) {
                    case 1:
                        showProductsAntCupsContent();
                        break;
                    case 2:
                        callOfServiceWorker = true;
                        break;
                    case 3:
                        exit("000");
                        break;
                }
            }
        } while (!callOfServiceWorker);
    }


    public void makeCoffee(String coffeeType, boolean bigCoffeeCup, int sugar) throws CoffeeMakingException {
        if ((!bigCoffeeCup && !this.availabilityToMakeSmallCoffeeCup.get(coffeeType)) ||
                (bigCoffeeCup && !this.availabilityToMakeBigCoffeeCup.get(coffeeType)) ||
                ((sugar != 0) && !this.products.getProducts().containsKey("Sugar")) ||
                ((sugar != 0) && ((sugar * (bigCoffeeCup ? this.bigCups.get(0).getCupCapacity() :
                        this.smallCups.get(0).getCupCapacity()) / 100) > this.products.getProducts().get("Sugar")))) {
            throw new CoffeeMakingException("\n\nPASIRINKTOS KAVOS PARUOSTI NEPAVYKO. KREIPKITES I SERVISO DARBUOTOJA.");
        } else {
            if (sugar > 0) {
                products.useIngredient("Sugar", (sugar * (bigCoffeeCup ? this.bigCups.get(0).getCupCapacity() :
                        this.smallCups.get(0).getCupCapacity()) / 100));
            }
            for (Map.Entry<String, Double> ingredient : this.coffeeTypes.getCoffeeTypeRecipe(coffeeType).entrySet()) {
                products.useIngredient(ingredient.getKey(), (ingredient.getValue() *
                        (bigCoffeeCup ? this.bigCups.get(0).getCupCapacity() : this.smallCups.get(0).getCupCapacity()) / 100));
            }
            if (bigCoffeeCup) {
                bigCups.remove(0);
            } else {
                smallCups.remove(0);
            }
            System.out.println("\n" + coffeeType + (bigCoffeeCup ? " didelis" : " mazas") + " puodelis paruostas. Skanaus!");
            checkAvailabilityToMakeSmallCoffeeCup();
            checkAvailabilityToMakeBigCoffeeCup();
        }
    }


    public void refillProductsIngredient(String ingredient, double ingredientContent) {
        this.products.refillIngredient(ingredient, ingredientContent);
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }

    public void refillAllProductsIngredients() {
        this.products.refillAllIngredients();
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }

    public void addNewProductsIngredient(String ingredient, double ingredientContent) {
        this.products.addNewIngredient(ingredient, ingredientContent);
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }

    public void removeProductsIngredient(String ingredient) {
        this.products.removeIngredient(ingredient);
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }


    public int addSmallCups(List<CoffeeCup> smallCups) {
        int counter = 0;
        for (CoffeeCup coffeeCup : smallCups) {
            if ((coffeeCup.getCupCapacity() >= MIN_SMALL_CUP_CAPACITY) && (coffeeCup.getCupCapacity() <= MAX_SMALL_CUP_CAPACITY)) {
                if (this.smallCups.size() < MAX_NUMBER_OF_SMALL_COFFEE_CUPS) {
                    counter++;
                    this.smallCups.add(coffeeCup);
                } else {
                    System.out.println("\nMazu puoduku talpykla pilna.");
                    break;
                }

            }
        }
        if (counter > 0) {
            System.out.println("\nIs viso buvo prideta mazu puoduku: " + counter + "vnt.");
            checkAvailabilityToMakeSmallCoffeeCup();
        } else {
            System.out.println("\nMazu puoduku talpykla nebuvo papildyta - pateikti puodukai nera tinkami.");
        }
        return counter;
    }

    public int addBigCups(List<CoffeeCup> bigCups) {
        int counter = 0;
        for (CoffeeCup coffeeCup : bigCups) {
            if ((coffeeCup.getCupCapacity() >= MIN_BIG_CUP_CAPACITY) && (coffeeCup.getCupCapacity() <= MAX_BIG_CUP_CAPACITY)) {
                if (this.bigCups.size() < MAX_NUMBER_OF_BIG_COFFEE_CUPS) {
                    counter++;
                    this.bigCups.add(coffeeCup);
                } else {
                    System.out.println("\nDideliu puoduku talpykla pilna.");
                    break;
                }

            }
        }
        if (counter > 0) {
            System.out.println("\nIs viso buvo prideta dideliu puoduku: " + counter + "vnt.");
            checkAvailabilityToMakeBigCoffeeCup();
        } else {
            System.out.println("\nDideliu puoduku talpykla nebuvo papildyta - pateikti puodukai nera tinkami.");
        }
        return counter;
    }

    public void removeSmallCups() {
        int removedNumber = this.smallCups.size();
        this.smallCups.clear();
        checkAvailabilityToMakeSmallCoffeeCup();
        System.out.println("\nIs viso buvo pasalinta mazu puoduku: " + removedNumber + "vnt.");
    }

    public void removeBigCups() {
        int removedNumber = this.bigCups.size();
        this.bigCups.clear();
        checkAvailabilityToMakeBigCoffeeCup();
        System.out.println("\nIs viso buvo pasalinta dideliu puoduku: " + removedNumber + "vnt.");
    }


    public void addNewCoffeeRecipe() {
        String newCoffeeType = inLine("\nIveskite naujos kavos pavadinima:");
        coffeeTypes.addNewCoffeeRecipe(newCoffeeType);
        this.availabilityToMakeSmallCoffeeCup.put(newCoffeeType, false);
        this.availabilityToMakeBigCoffeeCup.put(newCoffeeType, false);
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }

    public void removeCoffeeRecipe(String coffeeTypeToRemove) {
        if (this.coffeeTypes.getCoffeeTypes().containsKey(coffeeTypeToRemove)) {
            this.coffeeTypes.removeCoffeeRecipe(coffeeTypeToRemove);
            this.availabilityToMakeSmallCoffeeCup.remove(coffeeTypeToRemove);
            this.availabilityToMakeBigCoffeeCup.remove(coffeeTypeToRemove);
            checkAvailabilityToMakeSmallCoffeeCup();
            checkAvailabilityToMakeBigCoffeeCup();
        } else {
            System.out.println("\nTokios kavos rusies nera.");
        }
    }


    public void cleanCoffeeMaker() {
        this.useCounter = 0;
        Set<String> ingredientsNames = products.getProducts().keySet();
        this.products = new Products(0, 0);
        for (String ingredientName : ingredientsNames) {
            if (!(ingredientName.equals("Coffee beans")) && !(ingredientName.equals("Water"))) {
                this.products.addNewIngredient(ingredientName, 0);
            }
        }
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
        System.out.println("\nKavos aparatas isvalytas.");
    }


    public void showProductsAntCupsContent() {
        products.showProductsContent();
        System.out.println("\nMazu puoduku skaicius: " + getNumberOfSmallCups() + " vnt;\nDideliu puoduku skaicius: " +
                getNumberOfBigCups() + " vnt.");
    }


    public void checkAvailabilityToMakeSmallCoffeeCup() {
        if (!getNeedToCleanCoffeeMaker()) {
            if (this.smallCups.size() > 0) {
                for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                    boolean availabilityToMakeCoffee = true;
                    for (Map.Entry<String, Double> ingredient : coffeeType.getValue().entrySet()) {
                        if (this.products.getProducts().containsKey(ingredient.getKey())) {
                            if ((ingredient.getValue() * this.smallCups.get(0).getCupCapacity() / 100)
                                    < this.products.getProducts().get(ingredient.getKey())) {
                            } else {
                                availabilityToMakeCoffee = false;
                                break;
                            }
                        } else {
                            availabilityToMakeCoffee = false;
                            break;
                        }
                    }
                    this.availabilityToMakeSmallCoffeeCup.put(coffeeType.getKey(), availabilityToMakeCoffee);
                }
            } else {
                for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                    this.availabilityToMakeSmallCoffeeCup.put(coffeeType.getKey(), false);
                }
            }
        } else {
            for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                this.availabilityToMakeSmallCoffeeCup.put(coffeeType.getKey(), false);
            }
        }
    }

    public void checkAvailabilityToMakeBigCoffeeCup() {
        if (!getNeedToCleanCoffeeMaker()) {
            if (this.bigCups.size() > 0) {
                for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                    boolean availabilityToMakeCoffee = true;
                    for (Map.Entry<String, Double> ingredient : coffeeType.getValue().entrySet()) {
                        if (this.products.getProducts().containsKey(ingredient.getKey())) {
                            if ((ingredient.getValue() * this.bigCups.get(0).getCupCapacity() / 100)
                                    < this.products.getProducts().get(ingredient.getKey())) {
                            } else {
                                availabilityToMakeCoffee = false;
                                break;
                            }
                        } else {
                            availabilityToMakeCoffee = false;
                            break;
                        }
                    }
                    this.availabilityToMakeBigCoffeeCup.put(coffeeType.getKey(), availabilityToMakeCoffee);
                }
            } else {
                for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                    this.availabilityToMakeBigCoffeeCup.put(coffeeType.getKey(), false);
                }
            }
        } else {
            for (Map.Entry<String, Map<String, Double>> coffeeType : this.coffeeTypes.getCoffeeTypes().entrySet()) {
                this.availabilityToMakeBigCoffeeCup.put(coffeeType.getKey(), false);
            }
        }
    }


    public Products makeCurrentProductsCopy() {
        Products productsCopy = new Products(this.products.getProducts().get("Coffee beans"), this.products.getProducts().get("Water"));
        for (Map.Entry<String, Double> ingredient : this.products.getProducts().entrySet()) {
            if (!(ingredient.getKey().equals("Coffee beans")) && !(ingredient.getKey().equals("Water"))) {
                productsCopy.addNewIngredient(ingredient.getKey(), ingredient.getValue());
            }
        }
        return productsCopy;
    }


    public int getNumberOfSmallCups() {
        return this.smallCups.size();
    }

    public int getNumberOfBigCups() {
        return this.bigCups.size();
    }

    public List<CoffeeCup> getSmallCups() {
        return smallCups;
    }

    public List<CoffeeCup> getBigCups() {
        return bigCups;
    }

    public int getUseCounter() {
        return useCounter;
    }

    public boolean getNeedToCleanCoffeeMaker() {
        if ((this.USE_LIMIT - this.useCounter) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public CoffeeTypes getCoffeeTypes() {
        return coffeeTypes;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
        checkAvailabilityToMakeSmallCoffeeCup();
        checkAvailabilityToMakeBigCoffeeCup();
    }


}






