package lt.vcs.nikolajus.coffeMaker;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.TreeMap;

import static lt.vcs.nikolajus.AdditionalTools.*;

public class CoffeeTypes implements CoffeeTypesInterface {


    private Map<String, Map<String, Double>> coffeeTypes; //coffee recipes in milliliters for 100ml coffee cup.


    public CoffeeTypes() {
        this.coffeeTypes = new TreeMap<>();
        this.coffeeTypes.put("Black coffee", new TreeMap<>() {
            {
                put("Coffee beans", 8.0);
                put("Water", 90.0);
            }
        });
        this.coffeeTypes.put("Cappuccino", new TreeMap<>() {
            {
                put("Coffee beans", 6.6);
                put("Milk powder", 7.0);
                put("Water", 85.0);
            }
        });
        this.coffeeTypes.put("Chocolate", new TreeMap<>() {
            {
                put("Chocolate powder", 6.0);
                put("Milk powder", 3.0);
                put("Water", 86.0);
            }
        });
        this.coffeeTypes.put("Hot water", new TreeMap<>() {
            {
                put("Water", 97.0);
            }
        });
    }


    public void editCoffeeRecipe(String coffeeType) {
        if (this.coffeeTypes.containsKey(coffeeType)) {
            Map<String, Double> editedRecipe = new TreeMap<>();
            for (Map.Entry<String, Double> editingRecipe : this.coffeeTypes.get(coffeeType).entrySet()) {
                editedRecipe.put(editingRecipe.getKey(), inDouble("\nIveskite nauja kieki kavos ingridientui "
                        + editingRecipe.getKey() + ":"));
            }
            int choice = 0;
            do {
                choice = chooseItem("\nJei norite prideti daugiau ingridientu siam gerimui, spauskite \"1\"," +
                        " jei prideti nebenorite, spauskite \"2\":", 2);
                if (choice == 1) {
                    String newIngredient = inLine("\nIveskite naujo ingridiento pavadinima:");
                    if (!editedRecipe.containsKey(newIngredient)) {
                        editedRecipe.put(newIngredient, inDouble("\nIveskite ingridiento kieki:"));
                    } else {
                        System.out.println("\nToks ingridientas jau yra.");
                    }
                }
            } while (choice != 2);
            this.coffeeTypes.put(coffeeType, editedRecipe);
            System.out.println("\nKavos receptas pakoreguodas.");
        } else {
            System.out.println("\nTokios kavos rusies nera.");

        }
    }

    public void addNewCoffeeRecipe(String newCoffeeType) {
        Map<String, Double> newRecipe = new TreeMap<>();
        int choice = 0;
        do {
            if (newRecipe.size() != 0) {
                choice = chooseItem("\nJei norite prideti daugiau ingridientu siam gerimui, spauskite \"1\"," +
                        " jei prideti nebenorite, spauskite \"2\":", 2);
            }
            if ((choice == 1) || (newRecipe.size() == 0)) {
                String newIngredient = inLine("\nIveskite naujo ingridiento pavadinima:");
                if (!newRecipe.containsKey(newIngredient)) {
                    newRecipe.put(newIngredient, inDouble("\nIveskite ingridiento kieki:"));

                } else {
                    System.out.println("\nToks ingridientas jau yra.");
                }
            }
        } while (choice != 2);
        this.coffeeTypes.put(newCoffeeType, newRecipe);
        System.out.println("\nNaujas kavos receptas pridetas.");
    }

    public void removeCoffeeRecipe(String coffeeType) {
        this.coffeeTypes.remove(coffeeType);
        System.out.println("\nKavos receptas pasalintas.");
    }

    public void showCoffeeRecipes() {
        System.out.println("\n\nEsamu kavos receptu sarasas:");
        this.coffeeTypes.forEach((coffeeType, ingredients) -> {
            System.out.println("\n" + coffeeType + ":");

            int counter = 0;
            for (Map.Entry<String, Double> ingredient : ingredients.entrySet()) {
                counter++;
                String out;
                if (counter < ingredients.size()) {
                    out = ";";
                } else {
                    out = ".";
                }
                System.out.println(ingredient.getKey() + ": " + new DecimalFormat("#.##").format(ingredient.getValue())
                        .replace(".", ",") + " ml" + out);
            }

        });
    }


    public Map<String, Double> getCoffeeTypeRecipe(String coffeeType) {
        if (this.coffeeTypes.containsKey(coffeeType)) {
            return this.coffeeTypes.get(coffeeType);
        } else {
            System.out.println("\nTokios kavos rusies nera.");
            return null;
        }
    }

    public Map<String, Map<String, Double>> getCoffeeTypes() {
        return coffeeTypes;
    }

}
