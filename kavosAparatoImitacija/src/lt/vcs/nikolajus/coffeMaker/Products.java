package lt.vcs.nikolajus.coffeMaker;

import java.text.DecimalFormat;
import java.util.Map;
import java.util.TreeMap;

import static lt.vcs.nikolajus.AdditionalTools.calculatePercent;

public class Products {

    public final double COFFEE_BEANS_LIMIT = 1000; // in milliliters
    public final double WATER_LIMIT = 10000; // in milliliters
    public final double OTHER_INGREDIENTS_LIMIT = 500; // in milliliters
    public final int NUMBER_OF_SECTIONS_FOR_OTHER_INGREDIENTS = 5;
    private int numberOfSectionsUsed;
    private Map<String, Double> products;
    private Map<String, Boolean> productsIsOver;
    private Map<String, Boolean> lowContentProducts;


    public Products() {
        this(0, 0, 0, 0, 0);
    }

    public Products(double coffeeBeans, double water) {
        this.products = new TreeMap<>();
        this.productsIsOver = new TreeMap<>();
        this.lowContentProducts = new TreeMap<>();
        if (coffeeBeans > COFFEE_BEANS_LIMIT) {
            this.products.put("Coffee beans", COFFEE_BEANS_LIMIT);
            System.out.println("\nI kavos aparata telpa " + COFFEE_BEANS_LIMIT + "ml kavos pupeliu. Jums liko nepanaudoti" +
                    (coffeeBeans - COFFEE_BEANS_LIMIT) + "ml kavos pupeliu.");
        } else {
            this.products.put("Coffee beans", coffeeBeans);
        }
        if (water > WATER_LIMIT) {
            this.products.put("Water", WATER_LIMIT);
            System.out.println("\nI kavos aparata telpa " + WATER_LIMIT + "ml vandens. Jums liko nepanaudoti" +
                    (water - WATER_LIMIT) + "ml vandens.");
        } else {
            this.products.put("Water", water);
        }
        this.productsIsOver.put("Coffee beans", false);
        this.productsIsOver.put("Water", false);
        this.lowContentProducts.put("Coffee beans", false);
        this.lowContentProducts.put("Water", false);
    }

    public Products(double coffeeBeans, double water, double sugar, double milkPowder, double chocolatePowder) {
        this(coffeeBeans, water);
        if (sugar > OTHER_INGREDIENTS_LIMIT) {
            this.products.put("Sugar", OTHER_INGREDIENTS_LIMIT);
            System.out.println("\nI kavos aparata telpa " + OTHER_INGREDIENTS_LIMIT + "ml cukraus. Jums liko nepanaudoti" +
                    (sugar - OTHER_INGREDIENTS_LIMIT) + "ml cukraus.");
        } else {
            this.products.put("Sugar", sugar);
        }
        if (milkPowder > OTHER_INGREDIENTS_LIMIT) {
            this.products.put("Milk powder", OTHER_INGREDIENTS_LIMIT);
            System.out.println("\nI kavos aparata telpa " + OTHER_INGREDIENTS_LIMIT + "ml pieno milteliu. Jums liko nepanaudoti" +
                    (milkPowder - OTHER_INGREDIENTS_LIMIT) + "ml pieno milteliu.");
        } else {
            this.products.put("Milk powder", milkPowder);
        }
        if (chocolatePowder > OTHER_INGREDIENTS_LIMIT) {
            this.products.put("Chocolate powder", OTHER_INGREDIENTS_LIMIT);
            System.out.println("\nI kavos aparata telpa " + OTHER_INGREDIENTS_LIMIT + "ml sokolado milteliu. Jums liko nepanaudoti" +
                    (chocolatePowder - OTHER_INGREDIENTS_LIMIT) + "ml sokolado milteliu.");
        } else {
            this.products.put("Chocolate powder", chocolatePowder);
        }
        this.numberOfSectionsUsed = 3;
        this.productsIsOver.put("Sugar", false);
        this.productsIsOver.put("Milk powder", false);
        this.productsIsOver.put("Chocolate powder", false);
        this.lowContentProducts.put("Sugar", false);
        this.lowContentProducts.put("Milk powder", false);
        this.lowContentProducts.put("Chocolate powder", false);
        checkProductsContent();
    }


    public void refillIngredient(String ingredient, double ingredientContent) {
        if (this.products.containsKey(ingredient)) {
            double currentIngredientContent = this.products.get(ingredient);
            double ingredientLimit;
            switch (ingredient) {
                case "Coffee beans":
                    ingredientLimit = COFFEE_BEANS_LIMIT;
                    break;
                case "Water":
                    ingredientLimit = WATER_LIMIT;
                    break;
                default:
                    ingredientLimit = OTHER_INGREDIENTS_LIMIT;
                    break;
            }
            if ((currentIngredientContent + ingredientContent) > ingredientLimit) {
                this.products.put(ingredient, ingredientLimit);
                System.out.println("\nKavos aparate sio ingridiento telpa " + ingredientLimit + "ml. Kavos aparatas" +
                        " sekmingai papildytas, jums liko nepanaudoti " +
                        (ingredientContent - ingredientLimit) + "ml ingridiento.");

            } else {
                this.products.put(ingredient, (currentIngredientContent + ingredientContent));
                if (ingredientContent > 0) {
                    System.out.println("\nIngridientas sekmingai papildytas.");
                }
            }
            checkProductsContent();
        } else {
            System.out.println("\nTokio ingridiento kavos aparate nera.");
        }
    }

    public void refillAllIngredients() {
        Map<String, Double> oldIngredientsContent = this.products;
        this.products = new TreeMap<>();
        for (Map.Entry<String, Double> ingredient : oldIngredientsContent.entrySet()) {
            double ingredientLimit;
            switch (ingredient.getKey()) {
                case "Coffee beans":
                    ingredientLimit = COFFEE_BEANS_LIMIT;
                    break;
                case "Water":
                    ingredientLimit = WATER_LIMIT;
                    break;
                default:
                    ingredientLimit = OTHER_INGREDIENTS_LIMIT;
                    break;
            }
            products.put(ingredient.getKey(), ingredientLimit);
        }
        checkProductsContent();
        System.out.println("\nIs viso buvo papildyta:");
        int counter = 0;
        for (Map.Entry<String, Double> ingredient : this.products.entrySet()) {
            counter++;
            String out;
            if (counter < this.products.size()) {
                out = ";";
            } else {
                out = ".";
            }
            System.out.println(ingredient.getKey() + ": " + (new DecimalFormat("#.##").
                    format(ingredient.getValue() - oldIngredientsContent.get(ingredient.getKey()))
                    .replace(".", ",")) + " ml" + out);
        }
    }

    public void useIngredient(String ingredient, double ingredientContentToUse) {
        if (this.products.containsKey(ingredient)) {
            if (this.products.get(ingredient) > ingredientContentToUse) {
                this.products.put(ingredient, this.products.get(ingredient) - ingredientContentToUse);
                double ingredientLimit;
                switch (ingredient) {
                    case "Coffee beans":
                        ingredientLimit = COFFEE_BEANS_LIMIT;
                        break;
                    case "Water":
                        ingredientLimit = WATER_LIMIT;
                        break;
                    default:
                        ingredientLimit = OTHER_INGREDIENTS_LIMIT;
                        break;
                }
                if (this.products.get(ingredient) < ingredientLimit / 10) {
                    System.out.println("\nPASTABA: " + ingredient + " likutis kavos aparate yra mazesnis nei 10% (liko " +
                            new DecimalFormat("#.##").format(this.products.get(ingredient))
                                    .replace(".", ",") + " ml).");

                }
            } else {
                System.out.println("\nIngridiento likutis nepakankamas. Truksta:\n" + ingredient + ": " +
                        (Math.abs(ingredientContentToUse - this.products.get(ingredient))) + "ml.");
            }
            checkProductsContent();
        } else {
            System.out.println("\nTokio ingridiento kavos aparate nera.");
        }

    }

    public void addNewIngredient(String ingredient, double ingredientContent) {
        if (NUMBER_OF_SECTIONS_FOR_OTHER_INGREDIENTS > this.numberOfSectionsUsed) {
            if (!this.products.containsKey(ingredient)) {
                this.numberOfSectionsUsed++;
                this.lowContentProducts.put(ingredient, false);
                this.productsIsOver.put(ingredient, false);
                if (ingredientContent > OTHER_INGREDIENTS_LIMIT) {
                    this.products.put(ingredient, OTHER_INGREDIENTS_LIMIT);
                    System.out.println("\nI kavos aparata telpa " + OTHER_INGREDIENTS_LIMIT + "ml ingridiento. Kavos aparatas" +
                            " sekmingai papildytas. Jums liko nepanaudoti" +
                            (ingredientContent - OTHER_INGREDIENTS_LIMIT) + "ml ingridiento.");
                } else {
                    this.products.put(ingredient, ingredientContent);
                    if (ingredientContent > 0) {
                        System.out.println("\nKavos aparatas sekmingai papildytas.");
                    }
                }
                checkProductsContent();
            } else {
                System.out.println("\nToks ingridientas jau yra.");
            }
        } else {
            System.out.println("\nNera laisvos vietos naujo ingridiento patalpinimui. Ingridientas nebuvo pridetas.");
        }
    }

    public void removeIngredient(String ingredient) {
        if (this.products.containsKey(ingredient)) {
            if (ingredient.equals("Coffee beans")) {
                System.out.println("\nKavos pupeliu vieta skirta tik kavos pupelems, todel sio ingridiento pasalinti negalima.");
            } else if (ingredient.equals("Water")) {
                System.out.println("\nVandens talpikla skirta tik vandens saugojimui, todel sio ingridiento pasalinti negalima.");
            } else {
                this.products.remove(ingredient);
                this.productsIsOver.remove(ingredient);
                this.lowContentProducts.remove(ingredient);
                this.numberOfSectionsUsed--;
                System.out.println("\nIngridientas pasalintas.");
                checkProductsContent();
            }
        } else {
            System.out.println("\nTokio ingridiento kavos aparate nera.");
        }
    }


    public void showProductsContent() {
        System.out.println("\nEsamas kavos aparato produktu kiekis:");
        int counter = 0;
        for (Map.Entry<String, Double> ingredient : this.products.entrySet()) {
            counter++;
            String out;
            if (counter < this.products.size()) {
                out = ";";
            } else {
                out = ".";
            }
            double ingredientLimit;
            switch (ingredient.getKey()) {
                case "Coffee beans":
                    ingredientLimit = COFFEE_BEANS_LIMIT;
                    break;
                case "Water":
                    ingredientLimit = WATER_LIMIT;
                    break;
                default:
                    ingredientLimit = OTHER_INGREDIENTS_LIMIT;
                    break;
            }
            System.out.println(ingredient.getKey() + ": " + new DecimalFormat("#.##").format(ingredient.getValue())
                    .replace(".", ",") + " ml (" +
                    (calculatePercent(ingredient.getValue(), ingredientLimit)) + "%)" + out);

        }
    }

    public void checkProductsContent() {
        for (Map.Entry<String, Double> ingredient : this.products.entrySet()) {
            double ingredientLimit;
            double isOverBoundary;
            switch (ingredient.getKey()) {
                case "Coffee beans":
                    ingredientLimit = COFFEE_BEANS_LIMIT;
                    isOverBoundary = 10;
                    break;
                case "Water":
                    ingredientLimit = WATER_LIMIT;
                    isOverBoundary = 130;
                    break;
                default:
                    ingredientLimit = OTHER_INGREDIENTS_LIMIT;
                    isOverBoundary = 5;
                    break;
            }
            if (ingredient.getValue() < (ingredientLimit / 10)) {
                this.lowContentProducts.put(ingredient.getKey(), true);
            } else {
                this.lowContentProducts.put(ingredient.getKey(), false);
            }
            if (ingredient.getValue() < isOverBoundary) {
                this.productsIsOver.put(ingredient.getKey(), true);
            } else {
                this.productsIsOver.put(ingredient.getKey(), false);
            }
        }
    }


    public Map<String, Double> getProducts() {
        return products;
    }

    public Map<String, Boolean> getProductsIsOver() {
        return productsIsOver;
    }

    public Map<String, Boolean> getLowContentProducts() {
        return lowContentProducts;
    }

}
