package lt.vcs.nikolajus.coffeMaker;

import java.util.Map;

public interface CoffeeTypesInterface {

    public Map<String, Double> getCoffeeTypeRecipe(String coffeeType);

    public void showCoffeeRecipes();

}
