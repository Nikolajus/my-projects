package lt.vcs.nikolajus.coffeMaker;

public class CoffeeMakingException extends Exception {

    public CoffeeMakingException(String reason) {
        super(reason);
    }

}
