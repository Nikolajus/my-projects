package lt.vcs.nikolajus.foodToolsFactory;

import lt.vcs.nikolajus.coffeMaker.CoffeeMaker;
import lt.vcs.nikolajus.coffeMaker.Products;
import lt.vcs.nikolajus.foodToolsFactory.cup.CoffeeCup;
import lt.vcs.nikolajus.foodToolsFactory.cup.CupCapacities;

import java.text.DecimalFormat;
import java.util.*;

public class FoodToolsFactory {

    private Map<CupCapacities, List<CoffeeCup>> coffeeCups;

    public FoodToolsFactory() {
        this.coffeeCups = new TreeMap<>();
    }


    public CoffeeMaker createCoffeeMaker() {
        return new CoffeeMaker();
    }

    public CoffeeMaker[] createCoffeeMakers(int numberOfCoffeeMakers) {
        CoffeeMaker coffeeMakers[] = new CoffeeMaker[numberOfCoffeeMakers];
        for (int i = 0; i < numberOfCoffeeMakers; i++) {
            coffeeMakers[i] = new CoffeeMaker();
        }
        return coffeeMakers;
    }

    public static void cleanCoffeMakers(CoffeeMaker[] coffeeMakers) {
        Map<String, Double> ingredientsCleared = new HashMap<>();
        for (int i = 0; i < coffeeMakers.length; i++) {
            Products products = coffeeMakers[i].makeCurrentProductsCopy();
            coffeeMakers[i].cleanCoffeeMaker();
            for (Map.Entry<String, Double> ingredient : products.getProducts().entrySet()) {
                if (ingredientsCleared.containsKey(ingredient.getKey())) {
                    ingredientsCleared.put(ingredient.getKey(), ingredientsCleared.get(ingredient.getKey()) + ingredient.getValue());
                } else {
                    ingredientsCleared.put(ingredient.getKey(), ingredient.getValue());
                }
            }
        }
        System.out.println("\nIsvalytu produktu kiekis:");
        int counter = 0;
        for (Map.Entry<String, Double> ingredientCleared : ingredientsCleared.entrySet()) {
            counter++;
            String out;
            if (counter < ingredientsCleared.size()) {
                out = ";";
            } else {
                out = ".";
            }
            System.out.println(ingredientCleared.getKey() + ": " + new DecimalFormat("#.##")
                    .format(ingredientCleared.getValue()).replace(".", ",") + " ml" + out);
        }
    }


    public void makeNewCups(CupCapacities cupCapacity, int numberOfCups) {
        if (numberOfCups > 0) {
            List<CoffeeCup> newCups = new ArrayList<>();
            for (int i = 0; i < numberOfCups; i++) {
                newCups.add(new CoffeeCup(cupCapacity));
            }
            if (this.coffeeCups.containsKey(cupCapacity)) {
                List<CoffeeCup> oldCups = this.coffeeCups.get(cupCapacity);
                for (int j = 0; j < oldCups.size(); j++) {
                    newCups.add(oldCups.get(j));
                }
                this.coffeeCups.put(cupCapacity, newCups);
            } else {
                this.coffeeCups.put(cupCapacity, newCups);
            }
            System.out.println("\n" + cupCapacity.getCupCapacity() + " ml puodukai uzsakyti.");
        } else {
            System.out.println("\nPuodukai neuzsakyti.");
        }
    }

    public void showAvailableCups() {
        if (!this.coffeeCups.isEmpty()) {
            System.out.println("\nEsamu puoduku sarasas:");
            int iterCounter = 0;
            String out = ";";
            for (Map.Entry<CupCapacities, List<CoffeeCup>> cups : this.coffeeCups.entrySet()) {
                iterCounter++;
                if (iterCounter == this.coffeeCups.size()) out = ".";
                System.out.println("" + cups.getKey().getCupCapacity() + " ml: " + cups.getValue().size() + " vnt" + out);
            }
        } else {
            System.out.println("\nPuoduku sandelys yra tuscias.");
        }
    }

    public List<CoffeeCup> getCoffeeCups(CupCapacities cupCapacity, int numberOfCups) {
        List<CoffeeCup> coffeeCupsFromWarehouse = new ArrayList<>();
        if (this.coffeeCups.containsKey(cupCapacity)) {
            if (this.coffeeCups.get(cupCapacity).size() >= numberOfCups) {
                List<CoffeeCup> coffeeCupsLeft = this.coffeeCups.get(cupCapacity);
                for (int i = 0; i < numberOfCups; i++) {
                    coffeeCupsFromWarehouse.add(coffeeCupsLeft.remove(0));
                }
                if (coffeeCupsLeft.size() > 0) {
                    this.coffeeCups.put(cupCapacity, coffeeCupsLeft);
                } else {
                    this.coffeeCups.remove(cupCapacity);
                }
            } else {
                List<CoffeeCup> coffeeCupsLeft = this.coffeeCups.get(cupCapacity);
                for (int j = 0; j < this.coffeeCups.get(cupCapacity).size(); j++) {
                    coffeeCupsFromWarehouse.add(coffeeCupsLeft.remove(j));
                }
                System.out.println("Is sandelio paimtu puoduku skaicius: " + coffeeCupsFromWarehouse + "vnt. Daugiau pasirinktos talpos puoduku sandėlyje nera.");
                this.coffeeCups.remove(cupCapacity);
            }
        } else {
            System.out.println("\nTokios talpos puoduku sandelyje nera. Uzsakykite nauju puoduku gamybai.");
        }
        return coffeeCupsFromWarehouse;
    }

    public Map<CupCapacities, List<CoffeeCup>> getAllCoffeeCups() {
        return coffeeCups;
    }

}
