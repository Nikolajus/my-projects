package lt.vcs.nikolajus.foodToolsFactory.cup;

import java.util.Scanner;

public enum CupCapacities {

    _160ml(1, 160),
    _185ml(2, 185),
    _200ml(3, 200),
    _220ml(4, 220),
    _250ml(5, 250),
    _300ml(6, 300),
    _330ml(7, 330),
    _350ml(8, 350),
    _400ml(9, 400),
    _500ml(10, 500),
    _750ml(11, 750),
    _1000ml(12, 1000);

    private int cupCapacity;
    private int cupCapacityId;

    CupCapacities(int cupCapacityId, int cupCapacity) {
        this.cupCapacity = cupCapacity;
        this.cupCapacityId = cupCapacityId;
    }

    @Override
    public String toString() {
        return "" + cupCapacityId + ". " + cupCapacity + "ml";
    }

    public static void showAllAvailableCups() {
        System.out.println("\nGalimos puoduku talpos uzsakymui:");
        for (CupCapacities capacity : CupCapacities.values()) {
            System.out.println(capacity);
        }
    }

    public static CupCapacities getByIdOrCapacity(int idOrCapacity) {
        int internalId = idOrCapacity;
        while (true) {
            for (CupCapacities capacity : CupCapacities.values()) {
                if ((internalId == capacity.getCupCapacityId()) || (internalId == capacity.getCupCapacity())) {
                    return capacity;
                }
            }
            System.out.println("\n");
            for (CupCapacities capacity1 : CupCapacities.values()) {
                System.out.println(capacity1);
            }
            System.out.println("\nNeteisingai ivestas puoduko talpos numeris arba talpos skaicius. Pasirinkite vieno is auksciau pateiktu" +
                    " puoduko talpa atitinkanciu numeriu ir, ji ivede, spauskite Enter:");
            try {
                internalId = Integer.parseInt(new Scanner(System.in).next());
            } catch (Exception e) {
            }
        }
    }

    public int getCupCapacity() {
        return cupCapacity;
    }

    public int getCupCapacityId() {
        return cupCapacityId;
    }

}
