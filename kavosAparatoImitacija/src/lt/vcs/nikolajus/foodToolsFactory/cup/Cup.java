package lt.vcs.nikolajus.foodToolsFactory.cup;

public class Cup implements CupInterface {

    protected int cupCapacity;
    protected boolean cupIsReadyForUse;
    protected boolean cupIsUnusable;


    public Cup(CupCapacities cupCapacity) {
        this.cupCapacity = cupCapacity.getCupCapacity();
        this.cupIsReadyForUse = true;
        this.cupIsUnusable = false;
    }


    public void cleanCup() {
        if (!cupIsUnusable) {
            this.cupIsReadyForUse = true;
        } else {
            System.out.println("Cup is unusable.");
        }
    }

    public void squeezeCup() {
        this.cupIsUnusable = true;
    }


    public int getCupCapacity() {
        return this.cupCapacity;
    }

    public boolean getCupIsReadyForUse() {
        return this.cupIsReadyForUse;
    }

    public boolean getCupIsUnusable() {
        return cupIsUnusable;
    }


}
