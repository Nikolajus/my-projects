package lt.vcs.nikolajus.foodToolsFactory.cup;

public interface CupInterface {

    void cleanCup();
    void squeezeCup();
    boolean getCupIsReadyForUse();
    boolean getCupIsUnusable();

}
