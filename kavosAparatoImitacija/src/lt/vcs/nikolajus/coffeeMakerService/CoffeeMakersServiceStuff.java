package lt.vcs.nikolajus.coffeeMakerService;

import lt.vcs.nikolajus.coffeMaker.CoffeeMaker;
import lt.vcs.nikolajus.foodToolsFactory.FoodToolsFactory;
import lt.vcs.nikolajus.foodToolsFactory.cup.CupCapacities;

import java.text.DecimalFormat;
import java.util.Map;

import static lt.vcs.nikolajus.AdditionalTools.*;


public class CoffeeMakersServiceStuff implements CoffeeMakerServiceStaffInterface {


    public void serviceCoffeeMaker(CoffeeMaker coffeeMaker, FoodToolsFactory foodToolsFactory) {
        int choice = 0;
        do {
            choice = chooseItem("\n\nPASIRINKITE VIENA IS ZEMIAU PATEIKTU FUNKCIJU, IVESKITE FUNKCIJOS NUMERI IR" +
                    " PAPAUSKITE ENTER:\n\n1. Isvalyti kavos aparata;\n\n2. Pilnai papildydi visus esamus kavos aparato" +
                    " ingridientus;\n3. Papildyti pasirinkta esama kavos aparato ingridienta;\n4. Prideti nauja kavos aparato" +
                    " ingridienta;\n5. Pasalinti esama kavos aparato ingridienta;\n6. Paziureti visu ingridientu likuti;\n\n7. " +
                    "Prideti mazu puoduku is sandelio;\n8. Prideti dideliu puoduku is sandelio;\n9. Pasalinti mazus puodukus" +
                    " is kavos aparato;\n10. Pasalinti didelius puodukus is kavos aparato;\n11. Paziureti mazu puoduku likuti" +
                    " kavos aparate;\n12. Paziureti dideliu puoduku likuti kavos aparate;\n13. Paziureti puoduku kieki " +
                    "sandelyje;\n14. Uzsakyti nauju puoduku;\n\n15. Paziureti visu kavos aparato gerimu receptus;\n16. Paziureti" +
                    " vieno pasirinkto kavos aparato gerimo recepta;\n17. Pakoreguoti esama kavos aparato gerimo recepta;\n" +
                    "18. Prideti nauja kavos recepta;\n19. Pasalinti esama kavos aparato recepta;\n\n20. Pabaigti kavos" +
                    " aparato aptarnavima ir grizti i bendra meniu.\n\nPASIRINKITE VIENA IS AUKSCIAU PATEIKTU FUNKCIJU, " +
                    "IVESKITE FUNKCIJOS NUMERI IR PAPAUSKITE ENTER:", 20);
            switch (choice) {
                case 1:
                    cleanCoffeeMaker(coffeeMaker);
                    break;
                case 2:
                    refillAllCoffeeMakerIngredients(coffeeMaker);
                    break;
                case 3:
                    refillCoffeeMakerIngredient(coffeeMaker);
                    break;
                case 4:
                    addNewIngredient(coffeeMaker);
                    break;
                case 5:
                    removeCurrentIngredient(coffeeMaker);
                    break;
                case 6:
                    coffeeMaker.getProducts().showProductsContent();
                    break;
                case 7:
                    addSmallCupsToCoffeeMaker(foodToolsFactory, coffeeMaker);
                    break;
                case 8:
                    addBigCupsToCoffeeMaker(foodToolsFactory, coffeeMaker);
                    break;
                case 9:
                    removeSmallCups(coffeeMaker);
                    break;
                case 10:
                    removeBigCups(coffeeMaker);
                    break;
                case 11:
                    System.out.println("\nKavos aparate mazu puoduku yra: " + (coffeeMaker.getNumberOfSmallCups()) + " vnt.");
                    break;
                case 12:
                    System.out.println("\nKavos aparate dideliu puoduku yra: " + (coffeeMaker.getNumberOfBigCups()) + " vnt.");
                    break;
                case 13:
                    showAvailableCupsInWarehouse(foodToolsFactory);
                    break;
                case 14:
                    orderNewCups(foodToolsFactory);
                    break;
                case 15:
                    showAllCoffeeRecipes(coffeeMaker);
                    break;
                case 16:
                    showCoffeeTypeRecipe(coffeeMaker);
                    break;
                case 17:
                    editCoffeeTypeRecipe(coffeeMaker);
                    break;
                case 18:
                    addNewCoffeeTypeRecipe(coffeeMaker);
                    break;
                case 19:
                    removeCoffeeRecipe(coffeeMaker);
                    break;
                default:
                    break;
            }
        } while (choice != 20);
    }


    public void cleanCoffeeMaker(CoffeeMaker coffeeMaker) {
        coffeeMaker.cleanCoffeeMaker();
    }

    public void refillAllCoffeeMakerIngredients(CoffeeMaker coffeeMaker) {
        coffeeMaker.refillAllProductsIngredients();
    }

    public void refillCoffeeMakerIngredient(CoffeeMaker coffeeMaker) {
        coffeeMaker.refillProductsIngredient(inLine("\nIveskite ingredienta, kuri norite papildyti:"),
                Math.abs(inDouble("\nIveskite turimo ingridiento papildymui kieki:")));
    }

    public void addNewIngredient(CoffeeMaker coffeeMaker) {
        coffeeMaker.addNewProductsIngredient(inLine("\nIveskite naujo ingridiento pavadinima:"),
                Math.abs(inDouble("\nIveskite ingridiento kieki:")));
    }

    public void removeCurrentIngredient(CoffeeMaker coffeeMaker) {
        coffeeMaker.removeProductsIngredient(inLine("\nIveskite ingrediento, kuri norite pasalinti, pavadinima:"));
    }


    public void showAllCoffeeRecipes(CoffeeMaker coffeeMaker) {
        coffeeMaker.getCoffeeTypes().showCoffeeRecipes();
    }

    public void showCoffeeTypeRecipe(CoffeeMaker coffeeMaker) {
        String coffeeName = inLine("\nIveskite esamos kavos rusies pavadinima:");
        System.out.println("\n\n" + coffeeName + " kavos receptas:");
        int counter = 0;
        for (Map.Entry<String, Double> ingredient : coffeeMaker.getCoffeeTypes().getCoffeeTypeRecipe(coffeeName).entrySet()) {
            counter++;
            String out;
            if (counter < coffeeMaker.getCoffeeTypes().getCoffeeTypeRecipe(coffeeName).size()) {
                out = ";";
            } else {
                out = ".";
            }
            System.out.println(ingredient.getKey() + ": " + new DecimalFormat("#.##").format(ingredient.getValue())
                    .replace(".", ",") + " ml" + out);
        }
    }

    public void editCoffeeTypeRecipe(CoffeeMaker coffeeMaker) {
        coffeeMaker.getCoffeeTypes().editCoffeeRecipe(inLine("\nIveskite esamos kavos rusies, kurios recepta" +
                " norite pakoreguoti, pavadinima:"));
    }

    public void addNewCoffeeTypeRecipe(CoffeeMaker coffeeMaker) {
        coffeeMaker.addNewCoffeeRecipe();
    }

    public void removeCoffeeRecipe(CoffeeMaker coffeeMaker) {
        coffeeMaker.removeCoffeeRecipe(inLine("\nIveskite esamos kavos rusies, kurios recepta" +
                " norite pasalinti, pavadinima:"));
    }


    public void showAvailableCupsInWarehouse(FoodToolsFactory foodToolsFactory) {
        foodToolsFactory.showAvailableCups();
    }

    public void orderNewCups(FoodToolsFactory foodToolsFactory) {
        CupCapacities.showAllAvailableCups();
        foodToolsFactory.makeNewCups(CupCapacities.getByIdOrCapacity(chooseItem("\nPasirinkite vieno is auksciau pateiktu" +
                        " puoduko talpa atitinkanciu numeriu ir, ji ivede, spauskite Enter:", 12)),
                inInt("\nIveskite norimu pagaminti puoduku skaiciu:"));

    }

    public void addSmallCupsToCoffeeMaker(FoodToolsFactory foodToolsFactory, CoffeeMaker coffeeMaker) {
        if (!(foodToolsFactory.getAllCoffeeCups().isEmpty())) {
            foodToolsFactory.showAvailableCups();
            System.out.println("\nKavos aparato mazu puoduku talpa turi buti nuo " + coffeeMaker.MIN_SMALL_CUP_CAPACITY
                    + " iki " + coffeeMaker.MAX_SMALL_CUP_CAPACITY + "ml.");
            int choice = chooseItem("\nJei norite pasirinkti puodukus is pateikto saraso, spauskite \"1\"," +
                    " jei esami puodukai jums netinka, spauskite \"2\", ir paspauskite Enter:", 2);
            if (choice == 1) {
                CupCapacities cupCapacity = CupCapacities.getByIdOrCapacity(inInt("\nIveskite pasirinkto puoduko talpa:"));
                if (foodToolsFactory.getAllCoffeeCups().containsKey(cupCapacity)) {
                    if ((cupCapacity.getCupCapacity() >= coffeeMaker.MIN_SMALL_CUP_CAPACITY) &&
                            (cupCapacity.getCupCapacity() <= coffeeMaker.MAX_SMALL_CUP_CAPACITY)) {
                        int maxCapableNumber = coffeeMaker.MAX_NUMBER_OF_SMALL_COFFEE_CUPS - coffeeMaker.getNumberOfSmallCups();
                        int availableCups = foodToolsFactory.getAllCoffeeCups().get(cupCapacity).size();
                        int numberOfAddingCups;
                        String out = "\n";
                        if (maxCapableNumber > availableCups) {
                            numberOfAddingCups = availableCups;
                        } else {
                            numberOfAddingCups = maxCapableNumber;
                            out = "\nMazu puoduku talpykla pilnai uzpildyta. ";
                        }
                        coffeeMaker.addSmallCups(foodToolsFactory.getCoffeeCups(cupCapacity, numberOfAddingCups));
                    } else {
                        System.out.println("\nKavos aparato mazu puoduku talpa turi buti nuo " + coffeeMaker.MIN_SMALL_CUP_CAPACITY
                                + " iki " + coffeeMaker.MAX_SMALL_CUP_CAPACITY + "ml. Mazi puodukai nebuvo ideti i kavos aparata.");
                    }
                } else {
                    System.out.println("\nTokios talpos puoduku sandelyje nera. Mazi puodukai nebuvo ideti i kavos aparata.");
                }
            } else {
                System.out.println("\nMazi puodukai nebuvo ideti i kavos aparata.");
            }
        } else {
            System.out.println("\nSandelyje nera puoduku. Mazi puodukai nebuvo ideti i kavos aparata.");
        }
    }

    public void addBigCupsToCoffeeMaker(FoodToolsFactory foodToolsFactory, CoffeeMaker coffeeMaker) {
        if (!(foodToolsFactory.getAllCoffeeCups().isEmpty())) {
            foodToolsFactory.showAvailableCups();
            System.out.println("\nKavos aparato dideliu puoduku talpa turi buti nuo " + coffeeMaker.MIN_BIG_CUP_CAPACITY
                    + " iki " + coffeeMaker.MAX_BIG_CUP_CAPACITY + "ml.");
            int choice = chooseItem("\nJei norite pasirinkti puodukus is pateikto saraso, spauskite \"1\"," +
                    " jei esami puodukai jums netinka, spauskite \"2\", ir paspauskite Enter:", 2);
            if (choice == 1) {
                CupCapacities cupCapacity = CupCapacities.getByIdOrCapacity(inInt("\nIveskite pasirinkto puoduko talpa:"));
                if (foodToolsFactory.getAllCoffeeCups().containsKey(cupCapacity)) {
                    if ((cupCapacity.getCupCapacity() >= coffeeMaker.MIN_BIG_CUP_CAPACITY) &&
                            (cupCapacity.getCupCapacity() <= coffeeMaker.MAX_BIG_CUP_CAPACITY)) {
                        int maxCapableNumber = coffeeMaker.MAX_NUMBER_OF_BIG_COFFEE_CUPS - coffeeMaker.getNumberOfBigCups();
                        int availableCups = foodToolsFactory.getAllCoffeeCups().get(cupCapacity).size();
                        int numberOfAddingCups;
                        String out = "\n";
                        if (maxCapableNumber > availableCups) {
                            numberOfAddingCups = availableCups;
                        } else {
                            numberOfAddingCups = maxCapableNumber;
                            out = "\nDideliu puoduku talpykla pilnai uzpildyta. ";
                        }
                        coffeeMaker.addBigCups(foodToolsFactory.getCoffeeCups(cupCapacity, numberOfAddingCups));
                    } else {
                        System.out.println("\nKavos aparato dideliu puoduku talpa turi buti nuo " + coffeeMaker.MIN_BIG_CUP_CAPACITY
                                + " iki " + coffeeMaker.MAX_BIG_CUP_CAPACITY + "ml. Dideli puodukai nebuvo ideti i kavos aparata.");
                    }
                } else {
                    System.out.println("\nTokios talpos puoduku sandelyje nera. Dideli puodukai nebuvo ideti i kavos aparata.");
                }
            } else {
                System.out.println("\nDideli puodukai nebuvo ideti i kavos aparata.");
            }
        } else {
            System.out.println("\nSandelyje nera puoduku. Dideli puodukai nebuvo ideti i kavos aparata.");
        }
    }

    public void removeSmallCups(CoffeeMaker coffeeMaker) {
        coffeeMaker.removeSmallCups();
    }

    public void removeBigCups(CoffeeMaker coffeeMaker) {
        coffeeMaker.removeBigCups();
    }


}
