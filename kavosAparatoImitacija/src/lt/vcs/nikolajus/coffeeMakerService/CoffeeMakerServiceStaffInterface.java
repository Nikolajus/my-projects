package lt.vcs.nikolajus.coffeeMakerService;

import lt.vcs.nikolajus.foodToolsFactory.FoodToolsFactory;
import lt.vcs.nikolajus.coffeMaker.CoffeeMaker;

public interface CoffeeMakerServiceStaffInterface {

    void cleanCoffeeMaker(CoffeeMaker coffeeMaker);

    void refillAllCoffeeMakerIngredients(CoffeeMaker coffeeMaker);

    void addSmallCupsToCoffeeMaker(FoodToolsFactory foodToolsFactory, CoffeeMaker coffeeMaker);

    void addBigCupsToCoffeeMaker(FoodToolsFactory foodToolsFactory, CoffeeMaker coffeeMaker);

}
