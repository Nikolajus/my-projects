package lt.vcs.nikolajus;

import java.text.DecimalFormat;
import java.util.Scanner;

public class AdditionalTools {

    private static Scanner newScan() {
        return new Scanner(System.in);
    }

    public static String inLine(String query) {
        System.out.println(query);
        String sentence = newScan().nextLine();
        return sentence;
    }

    public static int inInt(String query) {
        int number = 0;
        boolean error = true;
        while (error) {
            System.out.println(query);
            String sNumber = newScan().nextLine();
            try {
                number = Integer.parseInt(sNumber.replace(" ", ""));
                error = false;
            } catch (Exception e) {
                if (sNumber.equals("")) {
                    System.out.println("\nJus nieko neivedete.");
                } else {
                    System.out.println("\nJus ivedete \"" + sNumber + "\" vietoj sveikojo skaiciaus.");
                }
            }
        }
        return number;
    }

    public static double inDouble(String query) {
        double number = 0;
        boolean error = true;
        while (error) {
            System.out.println(query);
            String sNumber = newScan().nextLine();
            String sNumberPoint = sNumber.replace(",", ".").replace(" ", "");
            String sNumberComma = sNumber.replace(".", ",").replace(" ", "");
            try {
                number = Double.parseDouble(sNumberPoint);
                error = false;
            } catch (Exception e) {
                try {
                    number = Double.parseDouble(sNumberComma);
                    error = false;
                } catch (Exception f) {
                    if (sNumber.equals("")) {
                        System.out.println("\nJus nieko neivedete.");
                    } else {
                        System.out.println("\nJus ivedete \"" + sNumber + "\" vietoj sveikojo skaiciaus.");
                    }
                }
            }
        }
        return number;
    }

    public static int chooseItem(String query, int itemMax) {  // choose item from "1" to users selected number
        int itemNo = 0;
        do {
            itemNo = inInt(query);
            if ((itemNo < 1) || (itemNo > itemMax)) {
                System.out.println("\nIvedete ne ta skaiciu.");
            }
        } while ((itemNo < 1) || (itemNo > itemMax));
        return itemNo;
    }

    public static int chooseItem(String query, int itemMin, int itemMax) { // choose item between users selected numbers
        int itemNo = 0;
        do {
            itemNo = inInt(query);
            if ((itemNo < itemMin) || (itemNo > itemMax)) {
                System.out.println("\nIvedete ne ta skaiciu.");
            }
        } while ((itemNo < itemMin) || (itemNo > itemMax));
        return itemNo;
    }

    public static String calculatePercent(double valueToCalculate, double _100PercentValue) {
        return new DecimalFormat("#.##").format((valueToCalculate * 100)
                / _100PercentValue).replace(".", ",");
    }

    public static void exit(String threeZeros) {
        if (threeZeros.equals("000")) {
            System.out.println("\n\nProgramos autorius Nikolajus linki jums grazios dienos! :) ");
            System.out.println("Paspauskite Enter, kad iseiti.");
            inLine("");
            System.exit(0);
        }
    }

}
