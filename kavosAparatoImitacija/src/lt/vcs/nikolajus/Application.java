package lt.vcs.nikolajus;

import lt.vcs.nikolajus.coffeMaker.CoffeeMaker;
import lt.vcs.nikolajus.coffeeMakerService.CoffeeMakersServiceStuff;
import lt.vcs.nikolajus.foodToolsFactory.FoodToolsFactory;


public class Application {

    public static void main(String[] args) {

        FoodToolsFactory abLietuvosGerimai = new FoodToolsFactory();
        CoffeeMaker kavosAparatas = abLietuvosGerimai.createCoffeeMaker();
        CoffeeMakersServiceStuff servisoDarbuotojas = new CoffeeMakersServiceStuff();

        while (true) {
            kavosAparatas.turnOnCoffeeMaker();
            servisoDarbuotojas.serviceCoffeeMaker(kavosAparatas, abLietuvosGerimai);
        }

    }

}
