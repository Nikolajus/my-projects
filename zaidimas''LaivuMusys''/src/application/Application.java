package application;

import static application.AdditionalUtils.*;

public class Application {

    public static void main(String[] args) {

        GameControl gameControl = new GameControl();
        int choice = 0;

        System.out.println("\nSVEIKI ATVYKE I ZAIDIMA \"LAIVU MUSIS\"");

        do {
            gameControl.startGame();
            choice = chooseItem("\nJei norite zaisti zaidima dar karta, spauskite \"1\", jei zaisti nebenorite, spauskite \"2\":", 2);
        } while (choice != 2);

        exit("000");

    }

}
