package application.ships;


import java.util.Map;
import java.util.TreeMap;

public class Ship {

    private ShipSize shipSize;
    private ShipLocator shipLocator;
    private Map<Integer, ShipStatus> shipStatus;
    private boolean shipIsDrowned;


    public Ship(ShipSize shipSize) {
        this.shipSize = shipSize;
        this.shipLocator = new ShipLocator(this.shipSize.getShipLength());
        this.shipStatus = new TreeMap<>();
        this.shipIsDrowned = false;
        for (int i = 1; i <= shipSize.getShipLength(); i++) {
            shipStatus.put(i, ShipStatus.WHOLE);
        }
    }

    public void shootAtShip(int[] coordinates) {
        if (!shipIsDrowned) {
            int partOfShip = 0;
            if (this.shipSize.getShipLength() == 1) {
                partOfShip = 1;
                shipStatus.put(partOfShip, ShipStatus.DROWNED);
                this.shipIsDrowned = true;

            } else {
                Map<Integer, int[]> shipLocation = shipLocator.getShipCoordinates();
                for (Map.Entry<Integer, int[]> shipPartLocation : shipLocation.entrySet()) {
                    if ((coordinates[0] == shipPartLocation.getValue()[0]) && (coordinates[1] == shipPartLocation.getValue()[1])) {
                        partOfShip = shipPartLocation.getKey();
                        break;
                    }
                }
                if (this.shipStatus.get(partOfShip) == ShipStatus.WHOLE) {
                    shipStatus.put(partOfShip, ShipStatus.SHOT);
                }
                this.shipIsDrowned = true;
                for (Map.Entry<Integer, ShipStatus> shipPartStatus: this.shipStatus.entrySet()) {
                    if (shipPartStatus.getValue() == ShipStatus.WHOLE) {
                        this.shipIsDrowned = false;
                        break;
                    }
                }
                if (this.shipIsDrowned) {
                    for (Map.Entry<Integer, ShipStatus> shipPartStatus: this.shipStatus.entrySet()) {
                        shipStatus.put(shipPartStatus.getKey(), ShipStatus.DROWNED);
                    }

                }


            }


        }



    }

    public ShipLocator getShipLocator() {
        return shipLocator;
    }

    public ShipSize getShipSize() {
        return shipSize;
    }

    public Map<Integer, ShipStatus> getShipStatus() {
        return shipStatus;
    }

    public boolean getShipIsDrowned() {
        return shipIsDrowned;
    }


}
