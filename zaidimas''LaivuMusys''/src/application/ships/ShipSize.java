package application.ships;

public enum ShipSize {

    SINGLE(1),
    DOUBLE(2),
    TRIPLE(3),
    QUADRUPLE(4);




    private int shipLength;

    ShipSize(int shipLength) {
        this.shipLength = shipLength;
    }

    public static ShipSize getByShipLenght (int shipLength) {
        for (ShipSize shipSizeCurrent: ShipSize.values()) {
            if (shipSizeCurrent.getShipLength() == shipLength) {
                return shipSizeCurrent;
            }
        }
        return null;
    }

    public int getShipLength() {
        return shipLength;
    }
}
