package application.ships;

import application.battleField.BattleField;

import java.util.Map;
import java.util.TreeMap;

public class ShipLocator {

    private Map<Integer, int[]> shipCoordinates; // int[] - 0 index is X, 1 - Y
    private int shipSize;
    private boolean allCoordinatesSetted;
    private int numberOfCoordinatesSetted;

    public ShipLocator(int shipSize) {
        this.shipCoordinates = new TreeMap<>();
        this.shipSize = shipSize;
        this.allCoordinatesSetted = false;
        this.numberOfCoordinatesSetted = 0;
    }


    public boolean setShipCoordinates(BattleField battleField, int[] coordinates) { //int[] 0 index is X, 1 - Y

        if (((coordinates[0] >= 0) && (coordinates[0] <= 9)) && ((coordinates[1] >= 0) && (coordinates[1] <= 9)) &&
                (battleField.getSeaField()[coordinates[0]][coordinates[1]].getShip() == null)) {

            if (this.numberOfCoordinatesSetted == 0) {

                if (checkIfAroundAreNoShipsExceptOneCoordinates(coordinates, coordinates, battleField)) {
                    this.numberOfCoordinatesSetted++;
                    this.shipCoordinates.put(1, new int[]{coordinates[0], coordinates[1]});
                    if (this.shipSize == this.numberOfCoordinatesSetted) {
                        this.allCoordinatesSetted = true;
                    }
                    return true;
                } else {
                    return false;
                }

            } else if (this.numberOfCoordinatesSetted > 0) {

                if (checkIfAroundAreNoShipsExceptOneCoordinates(coordinates, this.shipCoordinates.get(this.numberOfCoordinatesSetted), battleField)) {
                    this.numberOfCoordinatesSetted++;
                    this.shipCoordinates.put(this.numberOfCoordinatesSetted, new int[]{coordinates[0], coordinates[1]});
                    if (this.shipSize == this.numberOfCoordinatesSetted) {
                        this.allCoordinatesSetted = true;
                    }
                    return true;
                } else {
                    return false;
                }

            }

        } else {
            return false;
        }

        return false;
    }

    private boolean checkIfAroundAreNoShipsExceptOneCoordinates(int[] coordinatesOfShip, int[] coordinatesToExcept, BattleField battleField) {

        if (((coordinatesOfShip[0] + 1 <= 9) ? (((coordinatesOfShip[0] + 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] == coordinatesToExcept[1]))
                ? true : (battleField.getSeaField()[coordinatesOfShip[0] + 1][coordinatesOfShip[1]].getShip() == null)) : true) && //checkOnRight

                ((coordinatesOfShip[0] - 1 >= 0) ? (((coordinatesOfShip[0] - 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0] - 1][coordinatesOfShip[1]].getShip() == null)) : true) && //checkOnLeft

                ((coordinatesOfShip[1] + 1 <= 9) ? (((coordinatesOfShip[0] == coordinatesToExcept[0]) && (coordinatesOfShip[1] + 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0]][coordinatesOfShip[1] + 1].getShip() == null)) : true) && //checkUnder

                ((coordinatesOfShip[1] - 1 >= 0) ? (((coordinatesOfShip[0] == coordinatesToExcept[0]) && (coordinatesOfShip[1] - 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0]][coordinatesOfShip[1] - 1].getShip() == null)) : true) && //checkAbove

                (((coordinatesOfShip[0] + 1 <= 9) && (coordinatesOfShip[1] + 1 <= 9)) ? (((coordinatesOfShip[0] + 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] + 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0] + 1][coordinatesOfShip[1] + 1].getShip() == null)) : true) && //checkOnRightUnder

                (((coordinatesOfShip[0] - 1 >= 0) && (coordinatesOfShip[1] + 1 <= 9)) ? (((coordinatesOfShip[0] - 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] + 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0] - 1][coordinatesOfShip[1] + 1].getShip() == null)) : true) && //checkOnLeftUnder

                (((coordinatesOfShip[0] + 1 <= 9) && (coordinatesOfShip[1] - 1 >= 0)) ? (((coordinatesOfShip[0] + 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] - 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0] + 1][coordinatesOfShip[1] - 1].getShip() == null)) : true) && //checkOnRightAbove

                (((coordinatesOfShip[0] - 1 >= 0) && (coordinatesOfShip[1] - 1 >= 0)) ? (((coordinatesOfShip[0] - 1 == coordinatesToExcept[0]) && (coordinatesOfShip[1] - 1 == coordinatesToExcept[1]))
                        ? true : (battleField.getSeaField()[coordinatesOfShip[0] - 1][coordinatesOfShip[1] - 1].getShip() == null)) : true)) { //checkOnLeftAbove
            return true;
        } else {
            return false;
        }

    }


    public Map<Integer, int[]> getShipCoordinates() {
        return this.shipCoordinates;
    }

    public boolean getAllCoordinatesSetted() {
        return this.allCoordinatesSetted;
    }

    public void setNumberOfCoordinatesSetted(int numberOfCoordinatesSetted) {
        this.numberOfCoordinatesSetted = numberOfCoordinatesSetted;
    }
}
