package application.ships;

public enum ShipStatus {

    WHOLE (1),
    SHOT (2),
    DROWNED (3);


    private int shiStatusId;

    ShipStatus(int shiStatusId) {
        this.shiStatusId = shiStatusId;
    }

    public static ShipStatus getShipStatusById (int shiStatusId) {
        for (ShipStatus shipStatusCurrent: ShipStatus.values()) {
            if (shipStatusCurrent.getShiStatusId() == shiStatusId) {
                return shipStatusCurrent;
            }
        }
        return null;
    }

    public int getShiStatusId() {
        return this.shiStatusId;
    }

}
