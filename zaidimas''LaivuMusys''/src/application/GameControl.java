package application;

import application.battleField.BattleField;
import application.battleField.SeaSpot;
import application.battleField.SeaSpotView;
import application.ships.Ship;
import application.ships.ShipSize;

import java.util.Map;

import static application.AdditionalUtils.*;


public class GameControl {

    private BattleField opponentBattleField;
    private BattleField myBattleField;
    private Ship[] myShips; //index 0 - quadruple, 1-2 triple, 3-5 double, 6-9 - single ship
    private Ship[] opponentShips; //index 0 - quadruple, 1-2 triple, 3-5 double, 6-9 - single ship
    private SmartShooter smartShooter;


    public GameControl() {
        opponentBattleField = new BattleField(true);
        myBattleField = new BattleField(false);
        this.myShips = new Ship[10];
        this.opponentShips = new Ship[10];
        this.myShips[0] = new Ship(ShipSize.getByShipLenght(4));
        this.myShips[1] = new Ship(ShipSize.getByShipLenght(3));
        this.myShips[2] = new Ship(ShipSize.getByShipLenght(3));
        this.myShips[3] = new Ship(ShipSize.getByShipLenght(2));
        this.myShips[4] = new Ship(ShipSize.getByShipLenght(2));
        this.myShips[5] = new Ship(ShipSize.getByShipLenght(2));
        this.myShips[6] = new Ship(ShipSize.getByShipLenght(1));
        this.myShips[7] = new Ship(ShipSize.getByShipLenght(1));
        this.myShips[8] = new Ship(ShipSize.getByShipLenght(1));
        this.myShips[9] = new Ship(ShipSize.getByShipLenght(1));
        this.opponentShips[0] = new Ship(ShipSize.getByShipLenght(4));
        this.opponentShips[1] = new Ship(ShipSize.getByShipLenght(3));
        this.opponentShips[2] = new Ship(ShipSize.getByShipLenght(3));
        this.opponentShips[3] = new Ship(ShipSize.getByShipLenght(2));
        this.opponentShips[4] = new Ship(ShipSize.getByShipLenght(2));
        this.opponentShips[5] = new Ship(ShipSize.getByShipLenght(2));
        this.opponentShips[6] = new Ship(ShipSize.getByShipLenght(1));
        this.opponentShips[7] = new Ship(ShipSize.getByShipLenght(1));
        this.opponentShips[8] = new Ship(ShipSize.getByShipLenght(1));
        this.opponentShips[9] = new Ship(ShipSize.getByShipLenght(1));



        this.smartShooter = new SmartShooter(this.myBattleField);
    }

    public void startGame() {
        int numberOfMyDrownedShips = 0;
        int numberOfOpponentDrownedShips = 0;
        int choice1 = 0;
        do {
            setShips(this.myBattleField, this.myShips);
            choice1 = chooseItem("\nJei jums tinka toks laivu isdestymas, spauskit \"1\", jei laivus noretumete isdelioti is naujo, spauskite \"2\":", 2);
            if (choice1 != 1) {
                this.myBattleField = new BattleField(false);
                this.myShips[0] = new Ship(ShipSize.getByShipLenght(4));
                this.myShips[1] = new Ship(ShipSize.getByShipLenght(3));
                this.myShips[2] = new Ship(ShipSize.getByShipLenght(3));
                this.myShips[3] = new Ship(ShipSize.getByShipLenght(2));
                this.myShips[4] = new Ship(ShipSize.getByShipLenght(2));
                this.myShips[5] = new Ship(ShipSize.getByShipLenght(2));
                this.myShips[6] = new Ship(ShipSize.getByShipLenght(1));
                this.myShips[7] = new Ship(ShipSize.getByShipLenght(1));
                this.myShips[8] = new Ship(ShipSize.getByShipLenght(1));
                this.myShips[9] = new Ship(ShipSize.getByShipLenght(1));
                this.smartShooter = new SmartShooter(this.myBattleField);
            }
        } while (choice1 != 1);

        setShips(this.opponentBattleField, this.opponentShips);

        System.out.println("\n\nZAIDIMAS STARTUOJA!");
        inLine("\nPaspauskite Enter, kad pradeti.");
        showBattleField();
        do {
            boolean[] mySuccess = new boolean[2]; // 0 index - ship is drowned, 1 index - shot is successful
            boolean[] opponentSuccess = new boolean[2]; // 0 index - ship is drowned, 1 index - shot is successful
            do {
                mySuccess = shootAtShip(this.opponentBattleField, enterShipCoordinates("\nKad sauti, iveskite taikinio koordinates, ir paspauskite Enter:"));
                showBattleField();
                if (mySuccess[0]) {
                    numberOfOpponentDrownedShips++;
                }
            } while (mySuccess[1] && (numberOfOpponentDrownedShips < 10));
            if (numberOfOpponentDrownedShips == 10) {
                showBattleField();
                System.out.println("\nSveikiname, jus nugalejote!");
                break;
            }
            inLine("\nJus nepataikete. Paspauskite Enter, kad leisti priesininkui sauti:");
            String out = "\nPriesininkas sove: ";
            do {
                int[] targetCoordinates = smartShooter.setTargetCoordinates();
                opponentSuccess = shootAtShip(this.myBattleField, targetCoordinates);
                out  = out + intCoordinatesToString(targetCoordinates) + ", ";
                if (opponentSuccess[0]) {
                    numberOfMyDrownedShips++;
                }
            } while (opponentSuccess[1] && (numberOfMyDrownedShips < 10));
            if (numberOfMyDrownedShips == 10) {
                showBattleField();
                System.out.println(out.substring(0, out.length()-2) + ".");
                System.out.println("\nJus pralaimejote. Bandykite dar karta.");
                break;
            } else {
                showBattleField();
                System.out.println(out.substring(0, out.length()-2) + ".");
            }
        } while (true);
    }

    public void setShips(BattleField battleField, Ship[] ships) {
        boolean autoSet = battleField.isOpponentField();
        if (!autoSet) {
            int choice = chooseItem("\nPasirinkite laivu issidestyma.\nJei norite isdestyti laivus rankiniu budu," +
                    " spauskite \"1\",\njei norite, kad laivus jums isdeliuotu automatiskai kompiuteris, spauskite \"2\":", 2);
            autoSet = (choice == 1) ? false : true;
        }
        if (!autoSet) {
            showBattleField();
            System.out.println("\nPasirinkite savo laivu issidestyma. Laivai negali liestis vienas prie kito.");
        }
        for (int i = 0; i < ships.length; i++) {
            boolean allCoordinatesSetted = false;
            Ship ship = ships[i];
            do {
                int[] coordinates = new int[2];
                int direction = 0;
                for (int j = 0; j < ship.getShipSize().getShipLength(); j++) {
                    if (j == 0) {
                        boolean firstPartIsSetted = false;
                        do {
                            if (!autoSet) {
                                System.out.println();
                                coordinates = enterShipCoordinates("Iveskite " + ship.getShipSize().getShipLength()
                                        + "-gubo laivo " + ((ship.getShipSize().getShipLength() == 1) ? "" : "pirmos dalies ") + "koordinates:");
                            } else {
                                coordinates = new int[]{random(0, 9), random(0, 9)};
                            }
                            if (ship.getShipSize().getShipLength() > 1) {
                                if (!autoSet) {
                                    System.out.println();
                                    direction = chooseItem("Pasirinkite laivo krypi ir iveskite krypties numeri:\n1. I VIRSU;\n" +
                                            "2. I APACIA;\n3. I KAIRE;\n4. I DESINE.\nIveskite krypties numeri:", 4);
                                } else {
                                    direction = random(1, 4);
                                }
                            }
                            firstPartIsSetted = ship.getShipLocator().setShipCoordinates(battleField, coordinates);
                            if (!firstPartIsSetted && !autoSet) {
                                System.out.println("\nLAIVAS NETILPO.");
                                showBattleField();
                            }
                        } while (!firstPartIsSetted);
                    } else {
                        int[] coordinatesCurrent = new int[2];
                        switch (direction) {
                            case 1:
                                coordinatesCurrent = new int[]{coordinates[0], coordinates[1] - j};
                                break;
                            case 2:
                                coordinatesCurrent = new int[]{coordinates[0], coordinates[1] + j};
                                break;
                            case 3:
                                coordinatesCurrent = new int[]{coordinates[0] - j, coordinates[1]};
                                break;
                            case 4:
                                coordinatesCurrent = new int[]{coordinates[0] + j, coordinates[1]};
                                break;
                        }
                        boolean coordinatesSetted = ship.getShipLocator().setShipCoordinates(battleField, coordinatesCurrent);
                        if (!coordinatesSetted) {
                            ship.getShipLocator().setNumberOfCoordinatesSetted(0);
                            if (!autoSet) {
                                System.out.println("\nLAIVAS NETILPO.");
                                showBattleField();
                            }
                            break;
                        }
                    }
                }
                allCoordinatesSetted = ship.getShipLocator().getAllCoordinatesSetted();
                if (allCoordinatesSetted) {
                    Map<Integer, int[]> shipCoordinates = ship.getShipLocator().getShipCoordinates();
                    for (Map.Entry<Integer, int[]> shipCoordinatesCurrent : shipCoordinates.entrySet()) {
                        battleField.getSeaField()[shipCoordinatesCurrent.getValue()[0]][shipCoordinatesCurrent.getValue()[1]].setSeaSpotView(SeaSpotView.WHOLE_SHIP);
                        battleField.getSeaField()[shipCoordinatesCurrent.getValue()[0]][shipCoordinatesCurrent.getValue()[1]].setShip(ship);
                    }
                    if (!autoSet) {
                        showBattleField();
                    }
                }

            } while (!allCoordinatesSetted);
        }
        if (!battleField.isOpponentField() && autoSet) {
            showBattleField();
        }
    }

    public void showBattleField() {
        System.out.println("\n\n     Mano musio laukas          Priesininko musio laukas\n");
        String myBattleFieldView[][] = new String[12][12];
        String opponentBattleFieldView[][] = new String[12][12];
        String[] firsAndLastLine = new String[]{"   ", "A ", "B ", "C ", "D ", "E ", "F ", "G ", "H ", "I ", "J ", " "};
        for (int i = 0; i < 12; i++) {
            if ((i == 0) || (i == 11)) {
                for (int j = 0; j < 12; j++) {
                    myBattleFieldView[i][j] = firsAndLastLine[j];
                    opponentBattleFieldView[i][j] = firsAndLastLine[j];
                }
            } else {
                for (int k = 0; k < 12; k++) {
                    if ((k == 0) && (i == 10)) {
                        myBattleFieldView[i][k] = "" + i + " ";
                        opponentBattleFieldView[i][k] = "" + i + " ";
                    } else if ((k == 11) && (i == 10)) {
                        myBattleFieldView[i][k] = "" + i + " ";
                        opponentBattleFieldView[i][k] = "" + i + " ";
                    } else if (k == 0) {
                        myBattleFieldView[i][k] = " " + i + " ";
                        opponentBattleFieldView[i][k] = " " + i + " ";
                    } else if (k == 11) {
                        myBattleFieldView[i][k] = "" + i + "  ";
                        opponentBattleFieldView[i][k] = "" + i + "  ";
                    } else {
                        SeaSpotView myCurrentSeaSpotView = this.myBattleField.getSeaField()[k - 1][i - 1].getSeaSpotView();
                        SeaSpotView opponentCurrentSeaSpotView = this.opponentBattleField.getSeaField()[k - 1][i - 1].getSeaSpotView();
                        if (myCurrentSeaSpotView == SeaSpotView.SEA) {
                            myBattleFieldView[i][k] = "~ ";
                        } else if (myCurrentSeaSpotView == SeaSpotView.WHOLE_SHIP) {
                            myBattleFieldView[i][k] = "O ";
                        } else if (myCurrentSeaSpotView == SeaSpotView.SHOT_SHIP) {
                            myBattleFieldView[i][k] = "/ ";
                        } else if (myCurrentSeaSpotView == SeaSpotView.DROWNED_SHIP) {
                            myBattleFieldView[i][k] = "X ";
                        } else {
                            myBattleFieldView[i][k] = "* ";
                        }
                        if (opponentCurrentSeaSpotView == SeaSpotView.SEA) {
                            opponentBattleFieldView[i][k] = "~ ";
                        } else if (opponentCurrentSeaSpotView == SeaSpotView.WHOLE_SHIP) {
                            opponentBattleFieldView[i][k] = "~ ";
                        } else if (opponentCurrentSeaSpotView == SeaSpotView.SHOT_SHIP) {
                            opponentBattleFieldView[i][k] = "/ ";
                        } else if (opponentCurrentSeaSpotView == SeaSpotView.DROWNED_SHIP) {
                            opponentBattleFieldView[i][k] = "X ";
                        } else {
                            opponentBattleFieldView[i][k] = "* ";
                        }
                    }
                }
            }
        }
        for (int l = 0; l < 12; l++) {
            for (int m = 0; m < 25; m++) {
                if (m < 12) {
                    System.out.print(myBattleFieldView[l][m]);
                } else if ((m == 12)) {
                    System.out.print("     ");
                    if ((l == 0) || (l == 11)) {
                        System.out.print("  ");
                    }
                } else {
                    System.out.print(opponentBattleFieldView[l][m - 13]);
                }
            }
            System.out.println();
        }

    }

    public boolean[] shootAtShip(BattleField battleField, int[] coordinates) {
        boolean[] success = new boolean[2]; // 0 index - ship is drowned, 1 index - shot is successful
        SeaSpot seaSpot = battleField.getSeaField()[coordinates[0]][coordinates[1]];
        if (seaSpot.getSeaSpotView() == SeaSpotView.SEA) {
            seaSpot.setSeaSpotView(SeaSpotView.TRY);
        } else if (seaSpot.getSeaSpotView() == SeaSpotView.WHOLE_SHIP) {
            success[1] = true;
            seaSpot.getShip().shootAtShip(coordinates);
            if (!seaSpot.getShip().getShipIsDrowned()) {
                seaSpot.setSeaSpotView(SeaSpotView.SHOT_SHIP);
            } else {
                success[0] = true;
                for (Map.Entry<Integer, int[]> shipPartCoordinates : seaSpot.getShip().getShipLocator().getShipCoordinates().entrySet()) {
                    battleField.getSeaField()[shipPartCoordinates.getValue()[0]][shipPartCoordinates.getValue()[1]].setSeaSpotView(SeaSpotView.DROWNED_SHIP);
                }
            }
        }
        return success;
    }


}
