package application;

import application.battleField.BattleField;
import application.battleField.SeaSpotView;

import java.util.ArrayList;
import java.util.List;

import static application.AdditionalUtils.random;

public class SmartShooter {

    private List<int[]> unusableCoordinates;
    private List<int[]> shotShipCoordinates;
    private BattleField battleField;

    public SmartShooter(BattleField battleField) {
        unusableCoordinates = new ArrayList<>();
        shotShipCoordinates = new ArrayList<>();
        this.battleField = battleField;
    }

    public int[] setTargetCoordinates() {
        if (!unusableCoordinates.isEmpty()) {
            int[] lastCoordinates = new int[]{unusableCoordinates.get(unusableCoordinates.size() - 1)[0],
                    unusableCoordinates.get(unusableCoordinates.size() - 1)[1]};
            if (this.battleField.getSeaField()[lastCoordinates[0]][lastCoordinates[1]].getSeaSpotView()
                    == SeaSpotView.DROWNED_SHIP) {
                if (shotShipCoordinates.isEmpty()) {
                    saveAllAroundUnusableCoordinates(lastCoordinates);
                    return setNewRandomTarget();
                } else {
                    this.shotShipCoordinates.add(lastCoordinates);
                    for (int[] currentCoordinates : this.shotShipCoordinates) {
                        saveAllAroundUnusableCoordinates(currentCoordinates);
                    }
                    this.shotShipCoordinates.clear();
                    return setNewRandomTarget();
                }
            } else if ((this.battleField.getSeaField()[lastCoordinates[0]][lastCoordinates[1]].getSeaSpotView()
                    == SeaSpotView.SHOT_SHIP)) {
                this.shotShipCoordinates.add(lastCoordinates);
                if (this.shotShipCoordinates.size() == 1) {
                    return setOneOfFourTargets();
                } else {
                    return setOneOfTwoTargets();
                }
            } else if (!this.shotShipCoordinates.isEmpty()) {
                if (this.shotShipCoordinates.size() == 1) {
                    return setOneOfFourTargets();
                } else {
                    return setOneOfTwoTargets();
                }

            } else {
                return setNewRandomTarget();
            }
        } else {
            return setNewRandomTarget();
        }
    }

    private boolean checkIfCoordinatesIsUsable(int[] coordinates) {
        if ((coordinates[0] >= 0) && (coordinates[0] <= 9) && (coordinates[1] >= 0) && (coordinates[1] <= 9)) {
            for (int[] currentCorrdinates : this.unusableCoordinates) {
                if ((coordinates[0] == currentCorrdinates[0]) && (coordinates[1] == currentCorrdinates[1])) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private void saveAllAroundUnusableCoordinates(int[] coordinates) {
        saveUnusableCoordinates(new int[]{coordinates[0] + 1, coordinates[1]});
        saveUnusableCoordinates(new int[]{coordinates[0] - 1, coordinates[1]});
        saveUnusableCoordinates(new int[]{coordinates[0], coordinates[1] + 1});
        saveUnusableCoordinates(new int[]{coordinates[0], coordinates[1] - 1});
        saveUnusableCoordinates(new int[]{coordinates[0] + 1, coordinates[1] + 1});
        saveUnusableCoordinates(new int[]{coordinates[0] + 1, coordinates[1] - 1});
        saveUnusableCoordinates(new int[]{coordinates[0] - 1, coordinates[1] + 1});
        saveUnusableCoordinates(new int[]{coordinates[0] - 1, coordinates[1] - 1});

    }

    private void saveUnusableCoordinates(int[] coordinates) {
        if ((coordinates[0] >= 0) && (coordinates[0] <= 9) && (coordinates[1] >= 0) && (coordinates[1] <= 9)) {
            boolean newUnusableCoordinates = true;
            for (int[] currentUnusableCoordinates : this.unusableCoordinates) {
                if ((coordinates[0] == currentUnusableCoordinates[0]) && (coordinates[1] == currentUnusableCoordinates[1])) {
                    newUnusableCoordinates = false;
                    break;
                }
            }
            if (newUnusableCoordinates) {
                this.unusableCoordinates.add(coordinates);
            }
        }
    }

    private int[] setNewRandomTarget() {
        int[] targetCoordinates = new int[2];
        boolean success = false;
        do {
            targetCoordinates[0] = random(0, 9);
            targetCoordinates[1] = random(0, 9);
            success = checkIfCoordinatesIsUsable(targetCoordinates);
        } while (!success);
        this.unusableCoordinates.add(targetCoordinates);
        return targetCoordinates;
    }

    private int[] setOneOfFourTargets() {
        int[] targetCoordinates = new int[2];
        boolean success = false;
        do {
            int computerChoice = random(1, 4);
            switch (computerChoice) {
                case 1:
                    targetCoordinates[0] = this.shotShipCoordinates.get(0)[0] + 1;
                    targetCoordinates[1] = this.shotShipCoordinates.get(0)[1];
                    break;
                case 2:
                    targetCoordinates[0] = this.shotShipCoordinates.get(0)[0] - 1;
                    targetCoordinates[1] = this.shotShipCoordinates.get(0)[1];
                    break;
                case 3:
                    targetCoordinates[0] = this.shotShipCoordinates.get(0)[0];
                    targetCoordinates[1] = this.shotShipCoordinates.get(0)[1] + 1;
                    break;
                case 4:
                    targetCoordinates[0] = this.shotShipCoordinates.get(0)[0];
                    targetCoordinates[1] = this.shotShipCoordinates.get(0)[1] - 1;
                    break;
            }
            success = checkIfCoordinatesIsUsable(targetCoordinates);
        } while (!success);
        this.unusableCoordinates.add(targetCoordinates);
        return targetCoordinates;
    }

    private int[] setOneOfTwoTargets() {
        int[] targetCoordinates = new int[2];
        boolean success = false;
        int minX = 9;
        int maxX = 0;
        int minY = 9;
        int maxY = 0;
        for (int[] currentCoordinates : this.shotShipCoordinates) {
            if (minX > currentCoordinates[0]) {
                minX = currentCoordinates[0];
            }
            if (maxX < currentCoordinates[0]) {
                maxX = currentCoordinates[0];
            }
            if (minY > currentCoordinates[1]) {
                minY = currentCoordinates[1];
            }
            if (maxY < currentCoordinates[1]) {
                maxY = currentCoordinates[1];
            }
        }
        do {
            int computerChoice = random(1, 2);
            switch (computerChoice) {
                case 1:
                    if (this.shotShipCoordinates.get(0)[0] == this.shotShipCoordinates.get(1)[0]) {
                        targetCoordinates[0] = this.shotShipCoordinates.get(0)[0];
                        targetCoordinates[1] = minY - 1;
                    } else {
                        targetCoordinates[0] = minX - 1;
                        targetCoordinates[1] = this.shotShipCoordinates.get(0)[1];
                    }
                    break;
                case 2:
                    if (this.shotShipCoordinates.get(0)[0] == this.shotShipCoordinates.get(1)[0]) {
                        targetCoordinates[0] = this.shotShipCoordinates.get(this.shotShipCoordinates.size() - 1)[0];
                        targetCoordinates[1] = maxY + 1;
                    } else {
                        targetCoordinates[0] = maxX + 1;
                        targetCoordinates[1] = this.shotShipCoordinates.get(this.shotShipCoordinates.size() - 1)[1];
                    }
                    break;
            }
            success = checkIfCoordinatesIsUsable(targetCoordinates);
        } while (!success);
        this.unusableCoordinates.add(targetCoordinates);
        return targetCoordinates;
    }

}
