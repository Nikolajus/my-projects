package application;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author n.afanasenka
 */
public class AdditionalUtils {

    public static void out(Object txt) {
        System.out.println(txt);

    }

    private static Scanner newScan() {
        return new Scanner(System.in);
    }

    public static int inInt(String query) {
        int number = 0;
        boolean error = true;
        while (error) {
            out(query);
            String sNumber = newScan().nextLine();
            exit(sNumber);
            try {
                number = Integer.parseInt(sNumber.replace(" ", ""));
                error = false;
            } catch (Exception e) {
                if (sNumber.equals("")) {
                    out("\nJus nieko neivedete.");
                } else {
                    out("\nJus ivedete \"" + sNumber + "\" vietoj sveikojo skaiciaus.");
                }
            }
        }
        return number;
    }

    public static String inLine(String query) {
        out(query);
        String sentence = newScan().nextLine();
        exit(sentence);
        return sentence;
    }

    public static int[] enterShipCoordinates(String query) {
        int[] coordinates = new int[2];
        boolean success = false;
        do {
            out(query);
            String sCoordinates = newScan().nextLine().toUpperCase().replace(" ", "")
                    .replace(".", "").replace(",", "").replace(";", "");
            exit(sCoordinates);
            if (sCoordinates.length() <= 3) {
                int x = 0;
                int y = 0;
                if (sCoordinates.length() == 2) {
                    try {
                        y = Integer.parseInt(sCoordinates.substring(1)) - 1;
                        x = xStringCoordinateToInt(sCoordinates.substring(0, 1));
                        if (((x >= 0) && (x <= 9)) && ((y >= 0) && (y <= 9))) {
                            coordinates[0] = x;
                            coordinates[1] = y;
                            success = true;
                        } else {
                            out("\nTokios koordinates musio lauke nera.");
                        }
                    } catch (Exception e) {
                        try {
                            y = Integer.parseInt(sCoordinates.substring(0, 1)) - 1;
                            x = xStringCoordinateToInt(sCoordinates.substring(1));
                            if (((x >= 0) && (x <= 9)) && ((y >= 0) && (y <= 9))) {
                                coordinates[0] = x;
                                coordinates[1] = y;
                                success = true;
                            } else {
                                out("\nTokios koordinates musio lauke nera.");
                            }
                        } catch (Exception g) {
                            out("\nJus ivedete \"" + sCoordinates + "\" vietoje koordinaciu.");
                        }
                    }
                } else {
                    try {
                        y = Integer.parseInt(sCoordinates.substring(1)) - 1;
                        x = xStringCoordinateToInt(sCoordinates.substring(0, 1));
                        if (((x >= 0) && (x <= 9)) && ((y >= 0) && (y <= 9))) {
                            coordinates[0] = x;
                            coordinates[1] = y;
                            success = true;
                        } else {
                            out("\nTokios koordinates musio lauke nera.");
                        }
                    } catch (Exception e) {
                        try {
                            y = Integer.parseInt(sCoordinates.substring(0, 2)) - 1;
                            x = xStringCoordinateToInt(sCoordinates.substring(2));
                            if (((x >= 0) && (x <= 9)) && ((y >= 0) && (y <= 9))) {
                                coordinates[0] = x;
                                coordinates[1] = y;
                                success = true;
                            } else {
                                out("\nTokios koordinates musio lauke nera.");
                            }
                        } catch (Exception g) {
                            out("\nJus ivedete \"" + sCoordinates + "\" vietoje koordinaciu.");
                        }
                    }
                }

            } else {
                out("\nJus ivedete \"" + sCoordinates + "\" vietoje koordinaciu.");
            }
        } while (!success);
        return coordinates;
    }

    private static int xStringCoordinateToInt(String xCoordinate) {
        int x = -1;
        switch (xCoordinate) {
            case "A":
                x = 0;
                break;
            case "B":
                x = 1;
                break;
            case "C":
                x = 2;
                break;
            case "D":
                x = 3;
                break;
            case "E":
                x = 4;
                break;
            case "F":
                x = 5;
                break;
            case "G":
                x = 6;
                break;
            case "H":
                x = 7;
                break;
            case "I":
                x = 8;
                break;
            case "J":
                x = 9;
                break;
        }
        return x;
    }

    public static String intCoordinatesToString(int[] coordinates) {
        String out = "";
        switch (coordinates[0]) {
            case 0:
                out = "A";
                break;
            case 1:
                out = "B";
                break;
            case 2:
                out = "C";
                break;
            case 3:
                out = "D";
                break;
            case 4:
                out = "E";
                break;
            case 5:
                out = "F";
                break;
            case 6:
                out = "G";
                break;
            case 7:
                out = "H";
                break;
            case 8:
                out = "I";
                break;
            case 9:
                out = "J";
                break;
        }
        out = out + (coordinates[1]+1);
        return out;
    }

    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }

    public static int chooseItem(String query, int itemMax) {
        int itemNo = 0;
        do {
            itemNo = inInt(query);
            if ((itemNo < 1) || (itemNo > itemMax)) {
                out("\nIvedete ne ta skaiciu.");
            }
        } while ((itemNo < 1) || (itemNo > itemMax));
        return itemNo;
    }

    public static void exit(String threeZeros) {
        if (threeZeros.equals("000")) {
            out("\n\nProgramos autorius Nikolajus linki jums grazios dienos! :) ");
            out("Paspauskite Enter, kad iseiti.");
            inLine("");
            System.exit(0);
        }
    }

}
