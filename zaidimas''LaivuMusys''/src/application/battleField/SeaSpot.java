package application.battleField;

import application.ships.Ship;

public class SeaSpot {

    private final int [] coordinates;
    private final boolean opponentField; //if true, whole ships are invisible
    private SeaSpotView seaSpotView;
    private Ship ship;

    public SeaSpot(int [] coordinates, boolean opponentField) {
        coordinates = new int [2];
        this.coordinates = coordinates;
        this.opponentField = opponentField;
        this.seaSpotView = SeaSpotView.SEA;
    }

    public int [] getCoordinates() {
        return this.coordinates;
    }

    public boolean isOpponentField() {
        return opponentField;
    }

    public SeaSpotView getSeaSpotView() {
        return seaSpotView;
    }

    public void setSeaSpotView(SeaSpotView seaSpotView) {
        this.seaSpotView = seaSpotView;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
