package application.battleField;

public enum SeaSpotView {
    SEA (0),
    WHOLE_SHIP (1),
    SHOT_SHIP (2),
    DROWNED_SHIP (3),
    TRY (4);

    private int seaSpotViewId;

    SeaSpotView(int seaSpotViewId) {
        this.seaSpotViewId = seaSpotViewId;
    }

    public static SeaSpotView getSeaSpotViewById (int seaSpotViewId) {
        for (SeaSpotView seaSpotViewCurrent: SeaSpotView.values()) {
            if (seaSpotViewCurrent.getSeaSpotViewId() == seaSpotViewId) {
                return seaSpotViewCurrent;
            }
        }
        return null;
    }

    public int getSeaSpotViewId() {
        return this.seaSpotViewId;
    }

}
