

package application.battleField;

import application.ships.Ship;

public class BattleField {

    boolean opponentField; //if true, whole ships are invisible
    private SeaSpot seaField[][];
    

    public BattleField(boolean opponentField) {
        this.seaField = new SeaSpot[10][10];
        this.opponentField = opponentField;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                this.seaField[i][j] = new SeaSpot(new int [] {i, j}, this.opponentField);
            }
        }
    }

    public boolean isOpponentField() {
        return opponentField;
    }

    public SeaSpot[][] getSeaField() {
        return seaField;
    }

    public void setSeaField(int x, int y, Ship ship) {
        this.seaField[x][y].setShip(ship);
    }

}
