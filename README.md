# APIE PROJEKTUS #


### 1. Žaidimo "JĖGA" imitacija ###

2017.06 po Vilnius Coding School 6 savaičių pradedančiųjų programuotojų kurso, pasirinkęs Java, savarankiškai atlikau projektą "Žaidimo JĖGA imitacija".

Programa imituoja tikrąjį TV žaidimą "Jėga". Imitacija atliekama, vietoje atsitiktinai įpučiamų laimingų kamuoliukų atsitiktinai parenkant laimingus skaičius.

Šioje programoje galite įmituoti žaidimą, "nusipirkdami" tiek vieną arba daug bilietėlių vienam žaidimo tiražui (yra pasirenkamos spėjamos skaičių kombinacijos), 
tiek ir pabandyti žaisti su viena arba keliomis spėjamomis skaičių kombinacijomis kelis tiražus į priekį.

Programa sukurta taip, kad atrodo paprasta, pasirinkus vieną spėjamų kamuoliukų kombinaciją vienam tiražui. Tačiau, pasirinkus didesnę statymo sumą su keliomis spėjamomis skaičių kombinacijomis keliems tiražams, 
žaidimo pabaigoje aktyvuojasi žaidimo statistikų peržiūros galimybė - galimą pažiūrėti tiek bendrą spėjamų kamuoliukų kombinacijų ir laimėtų sumų statistiką, tiek ir atskirai spėjamų kamuoliukų kombinacijų 
statistiką visuose tiražuose. Panorėjus, laimėtus pinigus galima panaudoti sekančiam(-iems) tiražui(-ams) ir žaisti dar kartą.


### 2. Kavos aparato imitacija ###

2017.11 Vilnius Coding School organizuojamų 1 mėn. Java dieninių mokymų pradedantiesiems metu savarankiškai atlikau namų darbą "Kavos aparatas". 
Atlikdamas namų darbą, stengiausi paliesti kuo daugiau mokymosi medžiagos temų bei objektiškumo, todėl išsiplėčiau - namų darbas virto nedideliu projektu su kovos aparato ir jo atributų gamykla bei 
kavos aparatą aptarnaujančiu serviso darbuotoju. Todėl šį projektą pavadinau "Kavos aparato imitacija".


### 3. Konsolinis žaidimas "Laivų mūšis" ###

2017.11 Vilnius Coding School organizuojamų 1 mėn. Java dieninių mokymų pradedantiesiems metu savarankiškai atlikau baigiamąjį projektą - parašiau konsolinę žaidimo "Laivų mūšis" versiją.
Žaidime yra žaidžiama prieš kompiuterį. Galima pasirinkti savo laivų išdėstymą tiek pačiam, tiek ir leisti tai atlikti kompiuteriui.
Kompiuterio šaudymui į laivus yra sukurta išmanioji šaudyklė, kuri šaudo tik ten, kur yra laivų išsidėstymo tikimybė, o, "sužeidusi" laivą, savo sekančius šūvius skiria šio laivo nuskandinimui. 
Todėl, norint laimėti, reikia labai pasistengti. :)