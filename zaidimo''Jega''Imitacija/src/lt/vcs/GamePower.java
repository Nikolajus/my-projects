/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static lt.vcs.MyUtils.*;

/**
 *
 * @author n.afanasenka
 */
public class GamePower {

    private int nextDrawingNumber;
    private int mainBet;
    private int amountWonTotal;
    private boolean _50cent;
    private boolean allBetsTheSame;
    private boolean firstGame;
    private List<List<Integer>> betsCollectionsForDrawings;
    private List<int[]> userBallsCollectionsForDrawings;
    private List<Map<String, Integer>> singleCombinationStatistics;
    private List<Map<String, Integer>> singleCombinationWonStatistics;
    private List<Integer> singleCombinationWonTotal;
    private boolean[] singleCombination50centWon;

    public GamePower() {
        this.mainBet = 0;
        this.amountWonTotal = 0;
        this._50cent = false;
        this.firstGame = true;
        this.nextDrawingNumber = dayInYear();
        this.betsCollectionsForDrawings = new ArrayList<>();
        this.userBallsCollectionsForDrawings = new ArrayList<>();
        this.singleCombinationStatistics = new ArrayList<>();
        this.singleCombinationWonStatistics = new ArrayList<>();;
        this.singleCombinationWonTotal = new ArrayList<>();
    }

    public void Game() {
        while (true) {
            if (this.firstGame) {
                out("");
                out("ZAIDIMAS JEGA");
                out("");
                out("Sveiki atvyke i zaidima JEGA!");
            } else {
                out("\n\n\nSveiki sugrize i zaidima JEGA!");
            }
            out("Sekancio tirazo numeris yra " + this.nextDrawingNumber + ".");
            out("");
            start();
        }
    }

    private void start() {
        if (this.firstGame) {
            int choice = 0;
            while ((choice != 1) && (choice != 2)) {
                choice = inInt("1. Jei norite pradeti zaidima, spauskite \"1\";\n2. Jei "
                        + "norite perskaityti zaidimo taisykles, spauskite \"2\";\n"
                        + "3. Jei norite iseiti, iveskite tris nulius, ir paspauskite Enter:");
                if ((choice != 1) && (choice != 2)) {
                    out("");
                    out("Ivedete ne ta skaiciu.");
                }
            }
            if (choice == 2) {
                gameRules();
            }
        }
        betAndNumberOfCombinationsAndDrawings();
        userBallsCollections();
        drawings();
    }

    private void gameRules() {
        out("");
        out("ZAIDIMO TAISYKLES");
        out("");
        out("1. Pasirinkite bendra statymo suma. Suma gali buti pastatyta tiek"
                + " vienam tirazui, tiek ir keliems is eiles. Taip pat gali"
                + " buti pasirinkta tiek viena, tiek ir kelios spejamu"
                + " kamuoliuku skaiciu kombinacijos. Vienos skaiciu kimbinacijos"
                + " statymo suma vienam tirazui gali buti nuo 1 iki 10 Euru."
                + " Maksimaliai galima bendra statymo suma simtas tukstanciu euru,"
                + " ir gali buti virsyjama tik kai kuriais isimties atvejais.");
        out("2. Pasirinkite kamuoliuku kombinaciju, kurias ketinate speti, kieki.");
        out("3. Pasirinkite tirazu, kuriems statote savo suma, kieki.");
        out("   Pastaba: Jei pasirinksite didesni kamuoliuku kombinaciju ar tirazu"
                + " skaiciu, zaidimo pabaigoje jums aktyvuosis sio zaidimo laimetu"
                + " pinigu bei laimingu kombinaciju statistika.");
        out("4. Pasirinkite 6 kamuoliuku skaiciu kombinacija(-as) nuo "
                + "\"1\" iki \"30\".");
        out("5. Pradekite zaidima. Kiekvieno tirazo metu atsitiktine tvarka bus"
                + " isridenami 6 kamuoliukai su skaiciais bei vienas papildomas. "
                + "Jei atspesite bent 3 kamuoliukus, laimesite pinigini priza. "
                + "Piniginio prizo verte priklauso nuo atspetu kamuoliuku"
                + " skaiciaus bei statymo sumos vienam turazui - laimeta suma uz"
                + " atspetus kamuoliukus yra dauginama is pastatymo sumos "
                + "atitinkamam tirazui.");
        out("");
        out("6. Laimejimo suma:");
        out("I. Jei atspejate 6 pagrindinius kamuoliukus, laimite 100 000 Eur;");
        out("II. Jei atspejate 5 pagrindinius kamuoliukus bei viena papildoma"
                + " (5+), laimite 5 000 Eur;");
        out("III. Jei pagrinsinius 5 - 200 Eur;");
        out("IV. 4+ - 68 Eur;");
        out("V. 4 - 11 Eur;");
        out("VI. 3+ - 3,50 Eur;");
        out("VII. 3 - 1 Eur.");
        out("Pastaba: laimeta suma yra dauginama is pastatymo sumos"
                + " atitinkamam tirazui.");
        out("");
        out("7. Laimeta suma galite atsiimti arba is naujo statyti naujam"
                + " zaidimui.");
        out("");
        out("Pastaba: Noredami pabaigti zaidima, bet kuriuo metu galite ivesti tris"
                + " nulius ir paspausti Enter.");
        out("\n\nJei norite pradeti zaidima, paspauskite Enter.\nJei norite iseiti,"
                + " iveskite tris nulius, ir paspaskite Enter:");
        inLine();
        out("");
    }

    private void betAndNumberOfCombinationsAndDrawings() {
        int choice = 0;
        if (this.firstGame || ((this.firstGame == false) && (this.mainBet == 0))) {
            if (this.firstGame) {
                out("");
            }
            do {
                choice = inInt("1. Jei norite pasirinkti bendra statymo suma,"
                        + " spejamu kamuoliuku kombinaciju ir tirazu skaiciu,\nir"
                        + " leisti kompiuteriui paskirstyti statoma suma kiekvienam"
                        + " zaidimui po lygiai, spauskite \"1\";\n"
                        + "2. Jei norite pasirinkti"
                        + " statymo suma kiekvienai kombinacijai ir tirazui"
                        + " rankiniu budu, spauskite \"2\",\nir"
                        + " paspauskite Enter:");
                if ((choice != 1) && (choice != 2)) {
                    out("");
                    out("Ivedete ne ta skaiciu.");
                }
            } while ((choice != 1) && (choice != 2));
        } else {
            choice = 1;
        }
        if (choice == 1) {
            automaticBetAndNumberOfCombinationsAndDrawings();
        } else {
            manualBetAndNumberOfCombinationsAndDrawings();
        }
    }

    private void automaticBetAndNumberOfCombinationsAndDrawings() {
        int c;
        int d;
        int e;
        int choice = 0;
        boolean notFirstGameZeroBet = false;
        do {
            int numberOfCombinationsPerDrawing = 1;
            int numberOfDrawings = 1;
            if (this.mainBet == 0) {
                out("");
                while ((this.mainBet <= 0) || (this.mainBet > 100000)) {
                    this.mainBet = inInt("Iveskite bendra statymo suma eurais ir paspauskite Enter:");
                    out("");
                    if (this.mainBet <= 0) {
                        out("Neteisingai ivedete bendra statymo suma.");
                    } else if (this.mainBet > 100000) {
                        out("Bendra statymo suma negali virsyti 100 000 Eur.");
                    }
                }
                if (this.firstGame == false) {
                    notFirstGameZeroBet = true;
                }
            } else {
                if ((this.firstGame == false) && (choice == 0)) {
                    out("Bendra statymo suma: " + this.mainBet + " Eur.");
                }
            }
            if (this.mainBet > 1) {
                do {
                    numberOfCombinationsPerDrawing = inInt("Pastatytai " + this.mainBet
                            + " Eur sumai galite pasirinkti nuo 1 iki "
                            + this.mainBet + " kamuoliuku skaiciu kombinaciju."
                            + "\nIveskite kamuoliuku"
                            + " kombinaciju, kurias ketinate pasirinkti, skaiciu,"
                            + " ir paspauskite Enter:");
                    if ((numberOfCombinationsPerDrawing < 1)
                            || (numberOfCombinationsPerDrawing > this.mainBet)) {
                        out("");
                        out("Toks kombinaciju skaicius negalimas.");
                    }
                } while ((numberOfCombinationsPerDrawing < 1)
                        || (numberOfCombinationsPerDrawing > this.mainBet));

                int drawingsMax = this.mainBet / numberOfCombinationsPerDrawing;
                if (drawingsMax > 1) {
                    int drawingsMin = (drawingsMax + 9) / 10;
                    if (drawingsMin == (this.mainBet / (numberOfCombinationsPerDrawing
                            * 10)) && (this.mainBet % drawingsMin != 0)) {
                        drawingsMin = (drawingsMax / 10) + 1;
                    }

                    out("");
                    do {
                        numberOfDrawings = inInt("Pastatytai sumai ir pasirinktam kombinaciju skaiciui"
                                + " galite pasirinkti nuo " + drawingsMin + " iki "
                                + drawingsMax + " tirazu."
                                + "\nIveskite pageidaujama tirazu"
                                + " skaiciu ir paspauskite Enter:");
                        if ((numberOfDrawings < drawingsMin)
                                || (numberOfDrawings > drawingsMax)) {
                            out("");
                            out("Toks tirazu skaicius negalimas.");
                        }
                    } while ((numberOfDrawings < drawingsMin)
                            || (numberOfDrawings > drawingsMax));
                } else {
                    out("");
                    out("Su pasirinkta statymo suma ir kombinaciju skaiciumi galite "
                            + "zaisti tik viena tiraza.");
                }

            } else {
                out("");
                out("Su pasirinkta statymo suma galite pasirinkti tik viena "
                        + "skaiciu kombinacija vienam tirazui.");

            }
            int outputRemainder;
            do {
                c = 2;
                d = 3;
                e = 4;
                int betOfOneCombination = this.mainBet / (numberOfCombinationsPerDrawing
                        * numberOfDrawings);
                int betRemainder = this.mainBet % (betOfOneCombination
                        * numberOfCombinationsPerDrawing * numberOfDrawings);
                outputRemainder = betRemainder;
                this.betsCollectionsForDrawings.clear();
                for (int i = 0; i < numberOfDrawings; i++) {
                    List<Integer> bets = new ArrayList<>();
                    for (int j = 0; j < numberOfCombinationsPerDrawing; j++) {
                        if (betRemainder > 0) {
                            bets.add(betOfOneCombination + 1);
                            betRemainder--;
                        } else {
                            bets.add(betOfOneCombination);
                        }
                    }
                    this.betsCollectionsForDrawings.add(bets);
                }
                String out7 = " kiekviename tiraze";
                if (this.mainBet == 1) {
                    out7 = "";
                }
                out("");
                String out1 = "\nBendra statymo suma: " + this.mainBet + " Eur;"
                        + "\nTirazu skaicius: " + numberOfDrawings + ";"
                        + "\nKombinaciju" + out7 + " skaicius: "
                        + numberOfCombinationsPerDrawing + ";\n";
                String out2;
                if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing == 1)
                        && (this.mainBet == 1)) {
                    out2 = "Kombinacijos statymo suma yra 1 Eur.";
                } else if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing == 1)) {
                    out2 = "Kombinacijos statymo suma yra " + this.mainBet + " Eur.";
                } else if (outputRemainder == 0) {
                    out2 = "Kompiuterio paskirstyta statymo suma kiekvienai kombinacijai: "
                            + betOfOneCombination + " Eur.";
                } else {
                    out2 = "Kompiuterio paskirstyta statymo suma:\n";

                    String a;
                    String b;

                    switch (outputRemainder / numberOfCombinationsPerDrawing) {
                        case 0:
                            a = "";
                            break;
                        case 1:
                            if (numberOfCombinationsPerDrawing == 1) {
                                a = "Pirmo tirazo kombinacijos ";
                            } else {
                                a = "Pirmo tirazo kiekvienos kombinacijos ";
                            }
                            break;
                        case 2:
                            a = "Pirmo ir antro tirazo kiekvienos kombinacijos ";
                            break;
                        default:
                            a = "1-" + outputRemainder / numberOfCombinationsPerDrawing
                                    + "-o tirazo kiekvienos kombinacijos ";
                            break;
                    }

                    switch (outputRemainder % numberOfCombinationsPerDrawing) {
                        case 0:
                            if (numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0) {
                                if (numberOfCombinationsPerDrawing == 1) {
                                    b = "statymo suma yra " + (betOfOneCombination + 1) + " Eur,\n"
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo kombinacijos statymo suma yra "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "statymo suma yra " + (betOfOneCombination + 1) + " Eur,\n"
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo kiekvienos kombinacijos statymo suma yra "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else {
                                b = "statymo suma yra " + (betOfOneCombination + 1) + " Eur,\n"
                                        + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                        + "-o ir visu likusiu tirazu kiekvienos kombinacijos"
                                        + " statymo suma yra "
                                        + betOfOneCombination + " Eur.";
                            }
                            break;
                        case 1:
                            if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing == 2)) {
                                b = "Pirmos kombinacijos statymo suma yra "
                                        + (betOfOneCombination + 1) + " Eur, antros - "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing > 2)) {
                                b = "Pirmos kombinacijos statymo suma yra "
                                        + (betOfOneCombination + 1) + " Eur,"
                                        + " likusiu kombinaciju statymo suma - po "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 2) && (numberOfCombinationsPerDrawing == 2)) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur,\nantro "
                                            + "tirazo antros kombinacijos - "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur, pirmo "
                                            + "tirazo antros kombinacijos\nir antro "
                                            + "tirazo visu kombinaciju statymo suma -  po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if ((numberOfDrawings == 2) && (numberOfCombinationsPerDrawing > 2)) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur,"
                                            + "\nlikusiu antro tirazo kombinaciju statymo "
                                            + "suma - po " + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur, pirmo "
                                            + "tirazo likusiu kombinaciju\nir antro "
                                            + "tirazo visu kombinaciju statymo suma -  po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if (numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0) {
                                if (numberOfCombinationsPerDrawing == 2) {
                                    b = "ir paskutinio tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur,"
                                            + "\npaskutinio tirazo antros"
                                            + " kombiancijos statymo suma - "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "ir paskutinio tirazo pirmos kombinacijos statymo suma yra "
                                            + (betOfOneCombination + 1) + " Eur,\nlikusiu"
                                            + " paskutinio tirazo kimbinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir " + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo pirmos kombinacijos statymo suma yra "
                                            + +(betOfOneCombination + 1) + " Eur,\nvisu likusiu "
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos kombinacijos statymo suma yra "
                                            + +(betOfOneCombination + 1) + " Eur,\nvisu likusiu "
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            }
                            break;
                        case 2:
                            if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing == 3)) {
                                b = "Pirmos ir antros kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,\ntrecios"
                                        + " kombinacijos statymo suma - "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 1) && (numberOfCombinationsPerDrawing > 3)) {
                                b = "Pirmos ir antros kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,\nlikusiu"
                                        + " kombinaciju statymo suma - po "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 2) && (numberOfCombinationsPerDrawing == 3)) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo pirmos ir antros "
                                            + "kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nantro "
                                            + "tirazo trecios kombinacijos statymo suma - "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos ir antros "
                                            + "kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\npirmo "
                                            + "tirazo trecios kombinacijos ir antro tirazo"
                                            + " visu kombinaciju statymo suma - "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if ((numberOfDrawings == 2) && (numberOfCombinationsPerDrawing > 3)) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo pirmos ir antros kombinacijos"
                                            + " statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nlikusiu"
                                            + " antro tirazo kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmos tirazo pirmos ir antros kombinacijos"
                                            + " statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nlikusiu"
                                            + " pirmo ir visu antro tirazo kombinaciju statymo"
                                            + " suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if ((numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0)
                                    && (numberOfCombinationsPerDrawing == 3)) {
                                b = "ir paskutinio tirazo pirmos ir antros kombinacijos "
                                        + "statymo suma yra - po " + (betOfOneCombination + 1)
                                        + " Eur,\npaskutinio tirazo trecios kombinacijos statymo suma - "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0)
                                    && (numberOfCombinationsPerDrawing > 3)) {
                                b = "ir paskutinio tirazo pirmos ir antros kombinacijos"
                                        + " statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,"
                                        + "\npaskutinio tirazo likusiu "
                                        + "kombinaciju statymo suma - po "
                                        + betOfOneCombination + " Eur.";
                            } else if (numberOfCombinationsPerDrawing == 3) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir " + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo pirmos ir antros kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\ntrecios ir "
                                            + "visu likusiu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos ir antros kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\npirmo tirazo trecios ir "
                                            + "visu likusiu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir " + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo pirmos ir antros kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nvisu likusiu"
                                            + " sio ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo pirmos ir antros kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nvisu likusiu"
                                            + " sio ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            }
                            break;
                        default:
                            if ((numberOfDrawings == 1) && ((numberOfCombinationsPerDrawing - 1)
                                    == (outputRemainder % numberOfCombinationsPerDrawing))) {
                                b = "1-" + (outputRemainder % numberOfCombinationsPerDrawing)
                                        + "-os kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,\npaskutines"
                                        + " kombinacijos statymo suma - "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 1) && ((numberOfCombinationsPerDrawing - 1)
                                    > (outputRemainder % numberOfCombinationsPerDrawing))) {
                                b = "1-" + (outputRemainder % numberOfCombinationsPerDrawing)
                                        + "-os kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,\nlikusiu"
                                        + " kombinaciju statymo suma - po "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings == 2) && ((numberOfCombinationsPerDrawing - 1)
                                    == (outputRemainder % numberOfCombinationsPerDrawing))) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\npaskutines"
                                            + "antro tirazo kombinacijos statymo suma - "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\npirmo tirazo paskutines"
                                            + " ir antro tirazo visu kombinaciju statymo suma - "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if ((numberOfDrawings == 2) && ((numberOfCombinationsPerDrawing - 1)
                                    > (outputRemainder % numberOfCombinationsPerDrawing))) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir antro tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,"
                                            + "\nlikusiu antro tirazo kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,"
                                            + "\nlikusiu pirmo tirazo ir visu antro tirazo"
                                            + " kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else if ((numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0)
                                    && ((numberOfCombinationsPerDrawing - 1)
                                    == (outputRemainder % numberOfCombinationsPerDrawing))) {
                                b = "ir paskutinio tirazo 1-"
                                        + (outputRemainder % numberOfCombinationsPerDrawing)
                                        + "-os kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,\nlikusios "
                                        + "paskutinio tirazo kombinacijos statymo suma - "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfDrawings - ((outputRemainder
                                    / numberOfCombinationsPerDrawing) + 1) == 0)
                                    && ((numberOfCombinationsPerDrawing - 1)
                                    > (outputRemainder % numberOfCombinationsPerDrawing))) {
                                b = "ir paskutinio tirazo 1-"
                                        + (outputRemainder % numberOfCombinationsPerDrawing)
                                        + "-os kombinacijos statymo suma yra - po "
                                        + (betOfOneCombination + 1) + " Eur,"
                                        + "\npaskutinio tirazo likusiu"
                                        + " kombinaciju statymo suma - po "
                                        + betOfOneCombination + " Eur.";
                            } else if ((numberOfCombinationsPerDrawing - 1)
                                    == (outputRemainder % numberOfCombinationsPerDrawing)) {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir " + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\n"
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo paskutines"
                                            + " ir visu likusiu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\n"
                                            + "pirmo tirazo paskutines"
                                            + " ir visu likusiu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            } else {
                                if (outputRemainder > numberOfCombinationsPerDrawing) {
                                    b = "ir " + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nvisu likusiu "
                                            + ((outputRemainder / numberOfCombinationsPerDrawing) + 1)
                                            + "-o tirazo ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                } else {
                                    b = "Pirmo tirazo 1-"
                                            + (outputRemainder % numberOfCombinationsPerDrawing)
                                            + "-os kombinacijos statymo suma yra - po "
                                            + (betOfOneCombination + 1) + " Eur,\nvisu likusiu"
                                            + " pirmo ir kitu tirazu kombinaciju statymo suma - po "
                                            + betOfOneCombination + " Eur.";
                                }
                            }
                            break;
                    }

                    out2 = out2 + a + b;
                }
                do {
                    String ifRemainderNotZero;
                    if ((outputRemainder != 0) && (this.firstGame || notFirstGameZeroBet)) {
                        c = 4;
                        d = 5;
                        e = 6;
                        String out3;
                        if (((betOfOneCombination + 1) * numberOfCombinationsPerDrawing
                                * numberOfDrawings) > 100100) {
                            out3 = "EXTRA: ";
                        } else {
                            out3 = "";
                        }
                        ifRemainderNotZero = "\n2. " + out3
                                + "Jei noretumete padidinti jusu bendra statymo suma iki "
                                + ((betOfOneCombination + 1) * numberOfCombinationsPerDrawing
                                * numberOfDrawings) + " Eur\nir ja paskirstyti "
                                + "visoms kombiancijoms po "
                                + (betOfOneCombination + 1) + " Eur, spauskite \"2\";"
                                + "\n3. Jei norite pamazinti jusu bendra statymo suma iki "
                                + ((betOfOneCombination) * numberOfCombinationsPerDrawing
                                * numberOfDrawings) + " Eur\nir ja paskirstyti "
                                + "visoms kombiancijoms po "
                                + (betOfOneCombination) + " Eur, spauskite \"3\";";
                    } else {
                        ifRemainderNotZero = "";
                    }
                    String out4 = "";
                    String out5 = "\n" + c + ". Jei norite is naujo pasirinkti kombinaciju ir tirazu skaiciu"
                            + "\nesamai bendrai statymo sumai, bei leisti kompiuteriui "
                            + "automatiskai paskirstyti sia suma, spauskite \""
                            + c + "\";";
                    String out6 = " statomos sumos";
                    if ((this.firstGame == false) && (notFirstGameZeroBet == false)) {
                        out4 = " pasiimti visus laimetus pinigus ir";
                    }
                    if (this.mainBet == 1) {
                        out5 = "";
                        out6 = "";
                        d = d - 1;
                        e = e - 1;
                    }
                    choice = inInt(out1 + out2 + "\n\n1. Jei jums tinka toks" + out6
                            + " paskirstymas, spauskite \"1\";" + ifRemainderNotZero + out5
                            + "\n" + d + ". Jei norite" + out4 + " is naujo pasirinkti bendra statymo suma,"
                            + "\nkombinaciju ir tirazu skaiciu, bei leisti kompiuteriui "
                            + "automatiskai paskirstyti jusu statoma suma, spauskite \""
                            + d + "\";"
                            + "\n" + e + ". Jei norite" + out4 + " is naujo pasirinkti"
                            + " statymo suma kiekvienai kombinacijai ir tirazui"
                            + "\nrankiniu budu, spauskite \"" + e + "\","
                            + " ir paspauskite Enter:");
                    if ((choice < 1 || choice > e)) {
                        out("");
                        out("Ivedete ne ta skaiciu.");
                    }
                } while (choice < 1 || choice > e);
                if ((outputRemainder != 0) && (this.firstGame || notFirstGameZeroBet) && (choice == 2)) {
                    this.mainBet = (betOfOneCombination + 1)
                            * numberOfCombinationsPerDrawing * numberOfDrawings;
                }
                if ((outputRemainder != 0) && (this.firstGame || notFirstGameZeroBet) && (choice == 3)) {
                    this.mainBet = betOfOneCombination
                            * numberOfCombinationsPerDrawing * numberOfDrawings;
                }
            } while ((outputRemainder != 0) && (this.firstGame || notFirstGameZeroBet)
                    && ((choice == 2) || (choice == 3)));
            this.allBetsTheSame = false;
            if (outputRemainder == 0) {
                this.allBetsTheSame = true;
            }
            if (choice == e) {
                manualBetAndNumberOfCombinationsAndDrawings();
            }
            if (choice == d) {
                this.mainBet = 0;
                this._50cent = false;
                this.firstGame = true;
            }
            if (choice == c) {
                out("");
            }
        } while ((choice == d) || (choice == c));
    }

    private void manualBetAndNumberOfCombinationsAndDrawings() {
        int choice1 = 0;
        this.firstGame = true;
        do {
            this.betsCollectionsForDrawings.clear();
            this.mainBet = 0;
            this._50cent = false;
            int i = 0;
            int k = 0;
            int choice2 = 0;
            this.allBetsTheSame = true;
            boolean stopChecking = false;
            out("");
            do {
                i++;
                List<Integer> bets = new ArrayList<>();
                int j = 0;

                do {
                    j++;
                    out("");
                    int bet = 0;
                    do {
                        bet = inInt("Iveskite " + i + "-o tirazo " + j + "-os "
                                + "kombinacijos statymo suma nuo 1 iki 10 Eur"
                                + " ir paspauskite Enter:");
                        if ((bet < 1) || (bet > 10)) {
                            out("");
                            out("Neteisingai ivesta statymo suma.");
                        }
                    } while ((bet < 1) || (bet > 10));
                    if ((bets.size() >= 1) && (stopChecking == false)) {
                        if (bet != bets.get(bets.size() - 1)) {
                            stopChecking = true;
                            this.allBetsTheSame = false;
                        }
                        if ((this.betsCollectionsForDrawings.size() >= 1)
                                && (bet != this.betsCollectionsForDrawings.
                                        get(this.betsCollectionsForDrawings.size() - 1).get(0))) {
                            stopChecking = true;
                            this.allBetsTheSame = false;
                        }
                    }
                    bets.add(bet);
                    this.mainBet += bet;
                    if (i == 1) {
                        out("");
                        String out1 = "";
                        String out2;
                        do {
                            if (j > 1) {
                                out1 = "\nIs viso ivesta kombinaciju: " + j + ";"
                                        + "\nBendra statymo suma: " + this.mainBet
                                        + " Eur.\n";
                            }
                            if (j == 1) {
                                out2 = "\n2. Jei norite zaisti zaidima su viena kombinacija,"
                                        + " spauskite \"2\", ir paspauskite Enter:";
                            } else {
                                out2 = "\n2. Jei daugiau kombinaciju prideti nebenorite, "
                                        + " spauskite \"2\", ir paspauskite Enter:";
                            }
                            choice2 = inInt(out1 + "\n1. Jei norite prideti statymo suma"
                                    + " dar vienai kombinacijai, spauskite \"1\";" + out2);
                            if ((choice2 != 1) && (choice2 != 2)) {
                                out("");
                                out("Ivedete ne ta skaiciu.");
                            }
                        } while ((choice2 != 1) && (choice2 != 2));
                        if ((choice2 == 2) || (this.mainBet > 100000)) {
                            k = j;
                        }
                    }
                } while (k != j);
                this.betsCollectionsForDrawings.add(bets);
                int choice3 = 0;
                do {
                    choice3 = 0;
                    out("");
                    do {
                        String out3 = "";
                        String out4 = "";
                        String out5 = "";
                        if (i > 1) {
                            out3 = ("\nIs viso pasirinktu tirazu skaicius: " + i + ";"
                                    + "\nKombinaciju kiekviename tiraze skaicius: " + k + ";"
                                    + "\nBendra statymu suma: " + this.mainBet + " Eur.\n\n");
                        }
                        if (this.mainBet <= 100000) {
                            out4 = "1. Jei norite prideti daugiau tirazu, spauskite \"1\";\n";
                            if (i == 1) {
                                out5 = "2. Jei norite zaisti zaidima su vienu tirazu,"
                                        + " spauskite \"2\";\n";
                            } else {
                                out5 = "2. Jei daugiau tirazu prideti nebenorite, "
                                        + " spauskite \"2\";\n";
                            }
                            choice1 = inInt(out3 + out4 + out5
                                    + "3. Jei norite statymo suma kiekvienai kombinacijai ir tirazui "
                                    + "pasirinkti is naujo rankiniu budu, spauskite \"3\";"
                                    + "\n4. Jei norite is naujo pasirinkti bendra statymo suma,"
                                    + " spejamu kamuoliuku kombinaciju ir tirazu skaiciu, ir"
                                    + " leisti\nkompiuteriui automatiskai paskirstyti statoma suma kiekvienam"
                                    + " zaidimui po lygiai, spauskite \"4\", ir paspauskite Enter:");
                            if ((choice1 < 1) || (choice1 > 4)) {
                                out("");
                                out("Ivedete ne ta skaiciu.");
                            }
                        } else {
                            out("Ivedete maksimaliai galimu tirazu skaiciu: leidziama bendra"
                                    + " didziausia statymo suma - vienas milijonas euru.");
                            choice1 = 2;
                        }
                    } while ((choice1 < 1) || (choice1 > 4));
                    if (choice1 == 1) {
                        do {
                            out("");
                            String out6;
                            if (k == 1) {
                                out6 = "1. Jei norite prideti viena ar daugiau tirazu su tokia pacia"
                                        + " kombinacijos statymo suma kaip ir " + i + "-o"
                                        + " tirazo, spauskite \"1\";"
                                        + "\n2. Jei " + (i + 1) + "-o tirazo kombinacijai "
                                        + "norite pasirinkti kita statymo suma, spauskite "
                                        + "\"2\", ir paspauskite Enter:";
                            } else {
                                out6 = "1. Jei norite prideti viena ar daugiau tirazu su tokiomis paciomis"
                                        + " kombinaciju statymo sumomis kaip ir " + i + "-o"
                                        + " tirazo, spauskite \"1\";"
                                        + "\n2. Jei " + (i + 1) + "-o tirazo kombinacijoms "
                                        + "norite pasirinkti kitas statymo sumas, spauskite "
                                        + "\"2\", ir paspauskite Enter:";
                            }
                            choice3 = inInt(out6);
                            if ((choice3 != 1) && (choice3 != 2)) {
                                out("");
                                out("Ivedete ne ta skaiciu.");
                            }
                        } while ((choice3 != 1) && (choice3 != 2));
                        if (choice3 == 1) {
                            int l = 0;
                            int m = 0;
                            for (int n = 0; n < bets.size(); n++) {
                                m += bets.get(n);
                            }
                            int numberOfDrawingAvailable = (100100 - this.mainBet) / m;
                            out("");
                            do {
                                l = inInt("Iveskite pridedamu tirazu skaiciu ir paspauskite Enter:");
                                if (l < 1) {
                                    out("");
                                    out("Neteisingai ivedete pridedamu tirazu skaiciu.");
                                }
                                if (l > numberOfDrawingAvailable) {
                                    out("");
                                    out("Per didelis pridedamu tirazu skaicius: bendra statymo suma "
                                            + "negali virsyti 100 000 Eur. Su esama bendra statymo "
                                            + "ir pasirinkta kombinaciju suma galimai pridedamu tirazu skaicius:"
                                            + " ne daugiau kaip " + numberOfDrawingAvailable + ".");
                                }
                            } while (l < 1 || l > numberOfDrawingAvailable);
                            for (int o = 0; o < l; o++) {
                                this.betsCollectionsForDrawings.add(bets);
                            }
                            i += l;
                            this.mainBet += (m * l);
                            if (l == numberOfDrawingAvailable) {
                                choice1 = 2;
                            }
                        }
                    }
                } while (choice3 == 1);
            } while (choice1 == 1);
        } while (choice1 == 3);
        if (choice1 == 4) {
            this.mainBet = 0;
            automaticBetAndNumberOfCombinationsAndDrawings();
        }
    }

    private void userBallsCollections() {

        int choice1;
        String out1;
        String out2;
        String out3;
        String out4;
        String out5;
        if (this.betsCollectionsForDrawings.get(0).size() == 1) {
            out1 = "";
            out2 = "";
            out3 = ", ir paspauskite Enter:";
            out4 = "";
            out5 = "";
        } else {
            out1 = "s";
            out2 = " visas";
            out3 = ";";
            out4 = "\n3. Jei norite atskirai kiekvienai skaiciu kombinacijai "
                    + "pasirinkti ivedimo buda, spauskite \"3\", ir paspauskite Enter:";
            out5 = "\nIs viso turite ivesti kombinaciju: " + this.betsCollectionsForDrawings.get(0).size() + ".";
        }
        out("");
        do {
            choice1 = inInt("\nPasirinkite 6 kamuoliuku skaiciu kombinacija" + out1 + " nuo 1 iki 30." + out5 + "\n\n"
                    + "1. Jei pageidaujate" + out2 + " skaiciu kombinacija" + out1 + " ivesti "
                    + "rankiniu budu, spaskite \"1\";"
                    + "\n2. Jei norite, kad" + out2 + " skaiciu kombinacija" + out1 + " parinktu "
                    + "kompiuteris, spauskite \"2\"" + out3 + out4);
            if (!((choice1 == 1) || (choice1 == 2)
                    || ((this.betsCollectionsForDrawings.get(0).size() != 1) && (choice1 == 3)))) {
                out("");
                out("Ivedete ne ta skaiciu.");
            }
        } while (!((choice1 == 1) || (choice1 == 2)
                || ((this.betsCollectionsForDrawings.get(0).size() != 1) && (choice1 == 3))));
        int choice2 = 0;
        for (int i = 0; i < this.betsCollectionsForDrawings.get(0).size(); i++) {
            String out7;
            String out9;
            if (this.betsCollectionsForDrawings.get(0).size() == 1) {
                out7 = "";
            } else {
                out7 = " kombinacijos Nr. " + (i + 1);
            }
            if ((choice1 == 1) || (choice1 == 3)) {
                if (this.betsCollectionsForDrawings.get(0).size() == 1) {
                    out9 = "";
                } else {
                    out9 = "\n\nSkaiciu kombinacija Nr. " + (i + 1) + ":\n\n";
                }
                choice2 = 0;
                if (choice1 == 3) {
                    String out8;
                    if ((i > 0) && ((this.betsCollectionsForDrawings.get(0).size() - i) > 1)) {
                        out8 = ";\n3. Jei norite, kad visas likusias kombinacijas parinktu kompiuteris,"
                                + " spauskite \"3\", ir paspauskite Enter:";
                    } else {
                        out8 = ", ir paspauskite Enter:";
                    }
                    do {
                        choice2 = inInt(out9 + "1. Jei norite skaiciu kombinacija"
                                + " ivesti rankiniu budu, spauskite \"1\";"
                                + "\n2. Jei norite, kad skaiciu kombinacija parinktu"
                                + " kompiuteris, spauskite \"2\"" + out8);
                        if (!((choice2 == 1) || (choice2 == 2) || ((i > 0)
                                && ((this.betsCollectionsForDrawings.get(0).size() - i) > 1)
                                && (choice2 == 3)))) {
                            out("\nIvedete ne ta skaiciu.");
                        }
                    } while (!((choice2 == 1) || (choice2 == 2) || ((i > 0)
                            && ((this.betsCollectionsForDrawings.get(0).size() - i) > 1)
                            && (choice2 == 3))));
                }
                if (choice2 == 2) {
                    this.userBallsCollectionsForDrawings.add(randomSix(1, 30));
                } else if (choice2 == 3) {
                    this.userBallsCollectionsForDrawings.add(randomSix(1, 30));
                    choice1 = 2;
                } else {
                    if (this.betsCollectionsForDrawings.get(0).size() == 1) {
                        out("");
                    } else {
                        out("\n\nSkaiciu kombinacija Nr. " + (i + 1) + ":");
                    }
                    int[] array = new int[6];
                    String out10 = "";
                    for (int j = 0; j < 6; j++) {
                        int temp;
                        if (j > 0) {
                            out("");
                        }
                        do {
                            do {
                                temp = inInt("\n" + out10 + "Iveskite kamuoliuko Nr. " + (j + 1)
                                        + " skaiciu nuo 1 iki 30 ir paspauskite Enter:");
                                if ((temp < 1) || (temp > 30)) {
                                    out("\nNeteisingai ivedete kamuoliuko skaiciu.");
                                }
                            } while ((temp < 1) || (temp > 30));
                            if (array[0] > 0) {
                                boolean ok = true;
                                for (int l : array) {
                                    if (l == temp) {
                                        out("\nToks kamuoliukas jau buvo panaudotas.");
                                        ok = false;
                                        break;
                                    }
                                }
                                if (ok == true) {
                                    array[j] = temp;
                                    if (j == 5) {
                                        Arrays.sort(array);
                                    }
                                    if (array[0] > 0) {
                                        out10 = "Ivesti" + out7 + " skaiciai:\n";
                                        for (int m = 0; m < 6; m++) {
                                            if (array[m] > 0) {
                                                out10 = out10.concat("" + array[m] + "   ");
                                            }
                                        }
                                        out10 = out10.concat("\n\n");
                                    }
                                }
                            } else {
                                array[j] = temp;
                            }

                        } while (array[j] == 0);
                        if (j == 5) {
                            int choice3;
                            do {
                                out("");
                                choice3 = inInt("\n" + out10 + "1. Jei jums tinka ivesti"
                                        + out7 + " skaiciai, spauskite \"1\";"
                                        + "\n2. Jei noretumete ivesti skaicius is naujo,"
                                        + " spauskite \"2\", ir paspauskite Enter:");
                                if ((choice3 != 1) && (choice3 != 2)) {
                                    out("\nIvedete ne ta skaiciu.");
                                }
                            } while ((choice3 != 1) && (choice3 != 2));
                            if (choice3 == 2) {
                                Arrays.fill(array, 0);
                                out10 = "";
                                if (this.betsCollectionsForDrawings.get(0).size() == 1) {
                                    out("");
                                } else {
                                    out("\n\nSkaiciu kombinacija Nr. " + (i + 1) + ":");
                                }
                                j = -1;
                            }
                        }
                    }
                    this.userBallsCollectionsForDrawings.add(array);
                }
            } else {
                this.userBallsCollectionsForDrawings.add(randomSix(1, 30));
            }
            if ((choice1 == 2) || (choice2 == 2)) {
                String out6;
                if (this.betsCollectionsForDrawings.get(0).size() == 1) {
                    out6 = ":";
                } else {
                    out6 = " Nr. " + (i + 1) + ":";
                }
                out("\nKompiuterio parinkta skaiciu kombinacija" + out6);
                for (int k = 0; k < 6; k++) {
                    System.out.print("" + this.userBallsCollectionsForDrawings.get(i)[k] + "   ");
                }
            }
        }

    }

    private void drawings() {
        List<Drawing> drawings = new ArrayList<>();
        Map<String, Integer> combinationStatistics = new HashMap<>();
        Map<String, Integer> wonStatistics = new HashMap<>();
        boolean _50centWon = false;
        this.singleCombination50centWon = new boolean[this.userBallsCollectionsForDrawings.size()];
        out("\n\n\nZAIDIMAS STARTUOJA!!!\n");
        out("Jei esate pasirenge zaisti, spauskite Enter!");
        inLine();
        int limit = 10;
        String out10;
        if ((this.betsCollectionsForDrawings.size() == 1)
                || (this.userBallsCollectionsForDrawings.size() == 1)) {
            limit = 50;
        } else if ((this.betsCollectionsForDrawings.size() <= 3)
                || (this.userBallsCollectionsForDrawings.size() <= 3)) {
            limit = 20;
        }
        int choice1 = 0;
        int b = 0;
        int count1 = 0;
        int count3 = 0;
        do {
            count1++;
            for (int i = 0; i < this.betsCollectionsForDrawings.size(); i++) {
                if (count1 == 1) {
                    drawings.add(new Drawing((this.nextDrawingNumber + i),
                            this.betsCollectionsForDrawings.get(i), this.userBallsCollectionsForDrawings));
                }
                if ((this.betsCollectionsForDrawings.size() <= limit)
                        || ((this.betsCollectionsForDrawings.size() > limit)
                        && (drawings.get(i).getAmountWonTotal() > 0))) {
                    String out11;
                    if (this.betsCollectionsForDrawings.size() > limit) {
                        out11 = "laimingas ";
                    } else {
                        out11 = "";
                    }
                    out("\nZaidimo " + out11 + "tirazas Nr. " + drawings.get(i).getDrawingNumber() + ":");
                    System.out.print("Iskrite tirazo kamuoliukai (+ papildomas):");
                    for (int j = 0; j < 6; j++) {
                        System.out.print("   [" + drawings.get(i).getBallsCombiantion()[j] + "]");
                    }
                    System.out.print("   (+" + drawings.get(i).getBallAdditional() + ");\n");
                }
                for (int k = 0; k < this.userBallsCollectionsForDrawings.size(); k++) {
                    String out1;
                    String out2;
                    String out3;
                    String out4;
                    String out5;
                    String out6;
                    String out7;
                    String out8;
                    if (this.userBallsCollectionsForDrawings.size() > limit) {
                        out8 = "laimingos ";
                    } else {
                        out8 = "";
                    }

                    if (((this.betsCollectionsForDrawings.size() <= limit)
                            || ((this.betsCollectionsForDrawings.size() > limit)
                            && (drawings.get(i).getAmountWonTotal() > 0)))
                            && ((this.userBallsCollectionsForDrawings.size() <= limit)
                            || ((drawings.get(i).getAmountWonCollection()[k] > 0)
                            && (this.userBallsCollectionsForDrawings.size() > limit)))) {
                        if (this.userBallsCollectionsForDrawings.size() > 1) {
                            out1 = "kombinacijos Nr. " + (k + 1) + " ";
                        } else {
                            out1 = "";
                        }
                        System.out.print("Jusu " + out8 + out1 + "kamuoliukai:");
                        for (int l = 0; l < 6; l++) {
                            out6 = "";
                            out7 = "";
                            boolean temp = true;
                            for (int m = 0; m < 6; m++) {
                                if (this.userBallsCollectionsForDrawings.get(k)[l]
                                        == drawings.get(i).getBallsCombiantion()[m]) {
                                    out6 = "[";
                                    out7 = "]";
                                    temp = false;
                                    break;
                                }
                            }
                            if (temp && (this.userBallsCollectionsForDrawings.get(k)[l]
                                    == drawings.get(i).getBallAdditional())) {
                                out6 = "(+";
                                out7 = ")";
                            }
                            System.out.print("   " + out6
                                    + this.userBallsCollectionsForDrawings.get(k)[l] + out7);
                        }
                        if ((((drawings.get(i).getAmountWonCollection()[k]
                                % this.betsCollectionsForDrawings.get(i).get(k)) != 0)
                                && (this.betsCollectionsForDrawings.get(i).get(k) > 1))
                                || ((this.betsCollectionsForDrawings.get(i).get(k) == 1)
                                && (drawings.get(i).get50centCollection()[k] == true))) {
                            out3 = ",50";
                        } else {
                            out3 = "";
                        }
                        if (drawings.get(i).get50centCollection()[k] == true) {
                            out4 = ",50Eur";
                        } else {
                            out4 = " Eur";
                        }
                        if (drawings.get(i).getAmountWonCollection()[k] > 0) {
                            out2 = "laimejo: " + (drawings.get(i).getAmountWonCollection()[k]
                                    / this.betsCollectionsForDrawings.get(i).get(k)) + out3
                                    + " x " + this.betsCollectionsForDrawings.get(i).get(k)
                                    + " = " + drawings.get(i).getAmountWonCollection()[k]
                                    + out4;
                        } else {
                            out2 = "nieko nelaimejo";
                        }
                        boolean zero = false;
                        if (this.userBallsCollectionsForDrawings.size() > limit) {
                            for (int n = (drawings.get(i).getAmountWonCollection().length - 1);
                                    n > k; n--) {
                                if (drawings.get(i).getAmountWonCollection()[n] > 0) {
                                    zero = false;
                                    break;
                                } else {
                                    zero = true;
                                }
                            }
                        }
                        if (k == (this.userBallsCollectionsForDrawings.size() - 1) || zero) {
                            out5 = ".\n";
                        } else {
                            out5 = ";\n";
                        }
                        System.out.print("   - " + out2 + out5);
                    }
                    if (count1 == 1) {
                        if (i == 0) {
                            this.singleCombinationStatistics.add(drawings.get(i).getSingleCombinationStatistics().get(k));
                            this.singleCombinationWonStatistics.add(drawings.get(i).getSingleCombinationWonStatistics().get(k));
                            this.singleCombinationWonTotal.add(drawings.get(i).getAmountWonCollection()[k]);
                            this.singleCombination50centWon[k] = drawings.get(i).get50centCollection()[k];
                        } else {
                            Map<String, Integer> map1 = this.singleCombinationStatistics.get(k);
                            drawings.get(i).getSingleCombinationStatistics().get(k).forEach((w, v)
                                    -> map1.merge(w, v, Integer::sum));
                            this.singleCombinationStatistics.set(k, map1);
                            Map<String, Integer> map2 = this.singleCombinationWonStatistics.get(k);
                            drawings.get(i).getSingleCombinationWonStatistics().get(k).forEach((w, v)
                                    -> map2.merge(w, v, Integer::sum));
                            this.singleCombinationWonStatistics.set(k, map2);
                            this.singleCombinationWonTotal.set(k, (this.singleCombinationWonTotal.get(k)
                                    + drawings.get(i).getAmountWonCollection()[k]));
                            if (drawings.get(i).get50centCollection()[k]) {
                                if (this.singleCombination50centWon[k]) {
                                    this.singleCombination50centWon[k] = false;
                                    this.singleCombinationWonTotal.set(k,
                                            (this.singleCombinationWonTotal.get(k) + 1));
                                    Map<String, Integer> map3 = this.singleCombinationWonStatistics.get(k);
                                    map3.put("3+", (map3.get("3+") + 1));
                                    this.singleCombinationWonStatistics.set(k, map3);
                                } else {
                                    this.singleCombination50centWon[k] = true;
                                }
                            }
                        }

                    }
                }
                if ((drawings.get(i).getAmountWonTotal() > 0) && (count1 == 1)) {
                    this.amountWonTotal += drawings.get(i).getAmountWonTotal();
                    if (drawings.get(i).get_50cent() == true) {
                        if (_50centWon) {
                            _50centWon = false;
                            this.amountWonTotal++;
                            wonStatistics.put("3+", (wonStatistics.get("3+") + 1));
                        } else {
                            _50centWon = true;
                        }
                    }
                }
                if ((this.betsCollectionsForDrawings.size() <= limit)
                        || ((this.betsCollectionsForDrawings.size() > limit)
                        && (drawings.get(i).getAmountWonTotal() > 0))) {
                    if (drawings.get(i).getAmountWonTotal() > 0) {
                        String out9;
                        if (drawings.get(i).get_50cent()) {
                            out9 = " 50 ct.";
                        } else {
                            out9 = ".";
                        }
                        String out12;
                        if (this.betsCollectionsForDrawings.size() > 1) {
                            out12 = "Bendra tirazo laimeta suma: ";
                        } else {
                            out12 = "Is viso laimeta suma: ";
                        }
                        out(out12 + drawings.get(i).getAmountWonTotal() + " Eur" + out9);
                    } else if (this.betsCollectionsForDrawings.size() > 1) {
                        out("Siame tiraze jus nieko nelaimejote.");
                    } else {
                        out("Jus nieko nelaimejote. Bandykite dar karta.");
                    }
                    if (this.betsCollectionsForDrawings.size() > 1) {
                        if (this.amountWonTotal > 0) {
                            if (_50centWon) {
                                out10 = " 50 ct.";
                            } else {
                                out10 = ".";
                            }
                            out("Is viso laimeta suma: " + this.amountWonTotal + " Eur" + out10);
                        } else if (((this.betsCollectionsForDrawings.size() - 1) == i) && (this.amountWonTotal == 0)) {
                            out("\nJus nieko nelaimejote. Bandykite dar karta.");
                        }
                    }
                } else if (((this.betsCollectionsForDrawings.size() - 1) == i) && (this.amountWonTotal == 0)) {
                    out("\nJus nieko nelaimejote. Bandykite dar karta.");
                }
                if (count1 == 1) {
                    drawings.get(i).getCombinationStatistics().forEach((k, v)
                            -> combinationStatistics.merge(k, v, Integer::sum));
                    drawings.get(i).getWonStatistics().forEach((k, v)
                            -> wonStatistics.merge(k, v, Integer::sum));
                }
            }
            out("\n");
            int a = 1;
            b = 2;
            if (!((this.betsCollectionsForDrawings.size() > 3)
                    || ((this.userBallsCollectionsForDrawings.size() > 3) && (this.betsCollectionsForDrawings.size() > 1))
                    || (this.userBallsCollectionsForDrawings.size() > 10))) {
                a = 0;
                b = 1;
            }
            String out15 = "";
            String out18 = "";
            if ((this.betsCollectionsForDrawings.size() > limit) || (this.userBallsCollectionsForDrawings.size() > limit)) {
                String out16 = "laimingomis kombinacijomis ir laimingais tirazais.\n";
                String out17 = "visomis kombinacijomis ir visais tirazais, ";
                if (this.betsCollectionsForDrawings.size() == 1) {
                    out16 = "laimingomis kombinacijomis.\n";
                    out17 = "visais tirazais, ";
                } else if (this.userBallsCollectionsForDrawings.size() == 1) {
                    out16 = "laimingais tirazais.\n";
                    out17 = "visomis kombinacijomis, ";
                }
                out15 = "Jums buvo atvaizduoti zaidimo rezultatai tik su " + out16
                        + "1. Jei norite perziureti rezultatus su " + out17 + "spauskite \"1\";\n";
                a = 2;
                b = 3;
            }
            if ((this.betsCollectionsForDrawings.size() > 3)
                    || ((this.userBallsCollectionsForDrawings.size() > 3) && (this.betsCollectionsForDrawings.size() > 1))
                    || (this.userBallsCollectionsForDrawings.size() > 10)) {
                String out27 = "";
                if (count3 > 0) {
                    out27 = " dar karta";
                }
                out18 = "" + a + ". Jei norite" + out27 + " perziureti sio zaidimo kombinaciju ir laimetu sumu statistika, "
                        + "spauskite \"" + a + "\";\n";
            }
            if (b != 1) {
                do {
                    choice1 = inInt(out15 + out18 + b + ". Jei norite testi, spauskite \"" + b
                            + "\", ir paspauskite Enter.");
                    if ((choice1 < 1) || (choice1 > b)) {
                        out("");
                        out("Ivedete ne ta skaiciu.");
                    }
                    if ((a != 0) && (choice1 == a)) {
                        String out19;
                        String out20;
                        String out21;
                        String out22;
                        String out23;
                        String out24;
                        String out25 = "";
                        String out26 = ".";
                        double won3plus = wonStatistics.get("3+");
                        double thisAmountWonTotal = this.amountWonTotal;
                        count3++;
                        if (_50centWon) {
                            thisAmountWonTotal += 0.5;
                            out26 = " 50 ct.";
                        }
                        if (this.betsCollectionsForDrawings.size() == 1) {
                            out19 = ";";
                            out20 = "";
                            out22 = "";
                        } else {
                            out19 = " - " + (this.nextDrawingNumber
                                    + this.betsCollectionsForDrawings.size()) + ";";
                            out20 = ";\nTirazu skaicius: " + this.betsCollectionsForDrawings.size();
                            out22 = " visuose tirazuose";
                        }
                        if (this.userBallsCollectionsForDrawings.size() == 1) {
                            out21 = "";
                        } else {
                            out21 = ";\nKombinaciju skaicius: "
                                    + this.userBallsCollectionsForDrawings.size();
                        }
                        int sumOfCombinations = this.betsCollectionsForDrawings.size()
                                * this.userBallsCollectionsForDrawings.size();
                        if (this.allBetsTheSame) {
                            out23 = "" + this.betsCollectionsForDrawings.get(0).get(0);
                        } else {
                            out23 = "(statymo suma)";
                        }
                        out("\n\nZAIDIMO STATISTIKA:");
                        out("\nZaidimo tirazas Nr.: " + this.nextDrawingNumber + out19);
                        out("Bendra statymo suma: " + this.mainBet + " Eur" + out20 + out21 + ";");
                        out("Bendra laimeta suma: " + this.amountWonTotal + " Eur" + out26);
                        out("\n    Atspetu kamuoliuku kombinaciju ir laimetu sumu" + out22 + " statustika:");
                        out("0   Kombinaciju, kuriose nebuvo atspetas nei vienas kamuoliukas, skaicius: "
                                + combinationStatistics.get("0") + " ("
                                + calculatePercent(combinationStatistics.get("0"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        out("0+  Kombinaciju, kuriose buvo atspetas tik papildomas kamuoliukas: "
                                + combinationStatistics.get("0+") + " ("
                                + calculatePercent(combinationStatistics.get("0+"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        out("1   Kombinaciju, kuriose buvo atspetas 1 pagrindinis kamuoliukas: "
                                + combinationStatistics.get("1") + " ("
                                + calculatePercent(combinationStatistics.get("1"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        out("1+  Atspetas 1 pagrindinis ir papildomas kamuoliukas: "
                                + combinationStatistics.get("1+") + " ("
                                + calculatePercent(combinationStatistics.get("1+"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        out("2   Atspeti 2 pagrindiniai kamuoliukai: "
                                + combinationStatistics.get("2") + " ("
                                + calculatePercent(combinationStatistics.get("2"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        out("2+  Atspeti 2 pagrindiniai ir papildomas kamuoliukas: "
                                + combinationStatistics.get("2+") + " ("
                                + calculatePercent(combinationStatistics.get("2+"), sumOfCombinations)
                                + "%) - nieko nelaimejo;");
                        if (wonStatistics.get("3") > 0) {
                            out24 = " - laimejo 1 x " + out23 + " x "
                                    + combinationStatistics.get("3") + " = "
                                    + wonStatistics.get("3") + " Eur ("
                                    + calculatePercent(wonStatistics.get("3"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("3   Atspeti 3 kamuoliukai: "
                                + combinationStatistics.get("3") + " ("
                                + calculatePercent(combinationStatistics.get("3"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (_50centWon) {
                            out25 = " 50 ct";
                            won3plus += 0.5;
                        }
                        if (wonStatistics.get("3+") > 0) {
                            out24 = " - laimejo 3,50 x " + out23 + " x "
                                    + combinationStatistics.get("3+") + " = "
                                    + wonStatistics.get("3+") + " Eur" + out25 + " ("
                                    + calculatePercent(won3plus, thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("3+  Atspeti 3+ kamuoliukai: "
                                + combinationStatistics.get("3+") + " ("
                                + calculatePercent(combinationStatistics.get("3+"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (wonStatistics.get("4") > 0) {
                            out24 = " - laimejo 11 x " + out23 + " x "
                                    + combinationStatistics.get("4") + " = "
                                    + wonStatistics.get("4") + " Eur ("
                                    + calculatePercent(wonStatistics.get("4"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("4   Atspeti 4 kamuoliukai: "
                                + combinationStatistics.get("4") + " ("
                                + calculatePercent(combinationStatistics.get("4"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (wonStatistics.get("4+") > 0) {
                            out24 = " - laimejo 68 x " + out23 + " x "
                                    + combinationStatistics.get("4+") + " = "
                                    + wonStatistics.get("4+") + " Eur ("
                                    + calculatePercent(wonStatistics.get("4+"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("4+  Atspeti 4+ kamuoliukai: "
                                + combinationStatistics.get("4+") + " ("
                                + calculatePercent(combinationStatistics.get("4+"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (wonStatistics.get("5") > 0) {
                            out24 = " - laimejo 200 x " + out23 + " x "
                                    + combinationStatistics.get("5") + " = "
                                    + wonStatistics.get("5") + " Eur ("
                                    + calculatePercent(wonStatistics.get("5"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("5   Atspeti 5 kamuoliukai: "
                                + combinationStatistics.get("5") + " ("
                                + calculatePercent(combinationStatistics.get("5"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (wonStatistics.get("5+") > 0) {
                            out24 = " - laimejo 5000 x " + out23 + " x "
                                    + combinationStatistics.get("5+") + " = "
                                    + wonStatistics.get("5+") + " Eur ("
                                    + calculatePercent(wonStatistics.get("5+"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("5+  Atspeti 5+ kamuoliukai: "
                                + combinationStatistics.get("5+") + " ("
                                + calculatePercent(combinationStatistics.get("5+"), sumOfCombinations)
                                + "%)" + out24 + ";");
                        if (wonStatistics.get("6") > 0) {
                            out24 = " - laimejo 100000 x " + out23 + " x "
                                    + combinationStatistics.get("6") + " = "
                                    + wonStatistics.get("6") + " Eur ("
                                    + calculatePercent(wonStatistics.get("6"), thisAmountWonTotal) + "%)";
                        } else {
                            out24 = " - nieko nelaimejo";
                        }
                        out("6   Atspeti 6 kamuoliukai: "
                                + combinationStatistics.get("6") + " ("
                                + calculatePercent(combinationStatistics.get("6"), sumOfCombinations)
                                + "%)" + out24 + ".");
                        out("    Bendra laimeta suma: " + this.amountWonTotal + " Eur" + out26);
                        out("\n");
                        b = a;
                        if (b == 1) {
                            choice1 = b;
                        } else {
                            choice1 = 0;
                        }
                        a = 0;
                        out18 = "";

                        if ((this.userBallsCollectionsForDrawings.size() > 1)
                                && (this.amountWonTotal > 0)) {
                            int count2 = 0;
                            for (int i = 0; i < this.userBallsCollectionsForDrawings.size(); i++) {
                                if (this.singleCombinationWonTotal.get(i) > 0) {
                                    count2++;
                                }
                                if (count2 == 2) {
                                    break;
                                }
                            }
                            if ((count2 == 2) && (this.betsCollectionsForDrawings.size() > 1)) {
                                int choice2 = 0;
                                do {
                                    choice2 = inInt("1. Jei norite perziureti atskiru kombinaciju statistika"
                                            + " visuose zaidimo tirazuose, spauskite \"1\";\n2. Jei norite"
                                            + " testi, spauskite \"2\", ir paspauskite Enter:");
                                    if ((choice2 != 1) && (choice2 != 2)) {
                                        out("\nIvedete ne ta skaiciu.");
                                    }
                                } while ((choice2 != 1) && (choice2 != 2));
                                if (choice2 == 1) {
                                    singleCombinatonStatistic(out26);
                                }
                                out("\n");
                            }
                        }
                    }
                } while ((choice1 < 1) || (choice1 > b));
            }
            if (((this.betsCollectionsForDrawings.size() > 3)
                    || (this.userBallsCollectionsForDrawings.size() > 3)) && (choice1 == 1)) {
                limit = this.mainBet;
            }
            if (b != 1) {
                out("\n");
            }
        } while ((choice1 != b) && (b != 1));
        String out13 = ".";
        String out14 = "";
        String out27 = "";
        String out28 = "";
        String out29 = "";
        String out30 = " dar karta";
        int c = 1;
        int d = 2;
        if ((this.amountWonTotal > 0) || this._50cent) {
            if (this.amountWonTotal > 0) {
                if (_50centWon) {
                    if (this._50cent) {
                        this._50cent = false;
                        this.amountWonTotal++;
                    } else {
                        this._50cent = true;
                    }
                }
                c = 2;
                d = 3;
                out27 = "1. Jei norite pastatyti laimetus pinigus ir zaisti toliau, spauskite \"1\";\n";
            }
            if (this._50cent) {
                out13 = " 50 ct.";
            }
            out14 = "Is viso turite " + this.amountWonTotal + " Eur" + out13 + "\n";
            out28 = " pasiimti laimetus pinigus ir";
            out29 = " tik pasiimti laimetus pinigus ir";
            out30 = " is naujo";
        }
        int choice3 = 0;
        do {
            choice3 = inInt(out14 + out27 + c + ". Jei norite" + out28 + " atlikti" + out30 + " statyma bei zaisti"
                    + " toliau, spauskite \"" + c + "\";\n" + d + ". Jei norite" + out29 + " baigti zaidima,"
                    + " spauskite \"" + d + "\", ir paspauskite Enter:");
            if ((choice3 < 1) && (choice3 > d)) {
                out("\nIvedete ne ta skaiciu.");
            }
        } while ((choice3 < 1) || (choice3 > d));
        if ((this.amountWonTotal > 0) && (choice3 == 1)) {
            this.mainBet = this.amountWonTotal;
        } else if ((choice3 == c)) {
            this.mainBet = 0;
            this._50cent = false;
        } else {
            exit("000");
        }
        this.amountWonTotal = 0;
        this.nextDrawingNumber = (drawings.get(this.betsCollectionsForDrawings.size() - 1).getDrawingNumber() + 1);
        this.betsCollectionsForDrawings.clear();
        this.firstGame = false;
        this.singleCombinationStatistics.clear();
        this.singleCombinationWonStatistics.clear();
        this.singleCombinationWonTotal.clear();
        this.userBallsCollectionsForDrawings.clear();
    }

    private void singleCombinatonStatistic(String _50centWon) {
        String out1;
        if (this.allBetsTheSame) {
            out1 = "" + this.betsCollectionsForDrawings.get(0).get(0);
        } else {
            out1 = "(statymo suma)";
        }
        int l = 1;
        for (int k = 0; k < l; k++) {
            out("\n\nKOMBINACIJU STATISTIKA:");
            out("\nZaidimo tirazas Nr.: " + this.nextDrawingNumber + " - " + (this.nextDrawingNumber
                    + this.betsCollectionsForDrawings.size()) + ";");
            out("Bendra statymo suma: " + this.mainBet + " Eur" + ";\nTirazu skaicius: "
                    + this.betsCollectionsForDrawings.size() + ";\nKombinaciju skaicius: "
                    + this.userBallsCollectionsForDrawings.size() + ";");
            out("Bendra laimeta suma: " + this.amountWonTotal + " Eur" + _50centWon);
            double thisAmountWonTotal = this.amountWonTotal;
            if (_50centWon.equals(" 50 ct.")) {
                thisAmountWonTotal += 0.5;
            }
            for (int i = 0; i < this.userBallsCollectionsForDrawings.size(); i++) {
                if ((this.singleCombinationWonTotal.get(i) > 0) || (l == 2)) {
                    String out2;
                    String out3 = "";
                    double won3Plus = this.singleCombinationWonStatistics.get(i).get("3+");
                    double thisCombinationWonTotal = this.singleCombinationWonTotal.get(i);
                    if (this.singleCombination50centWon[i]) {
                        thisCombinationWonTotal += 0.5;
                    }

                    System.out.print("\n    Kombinacija Nr. " + (i + 1) + ":\n ");
                    for (int j = 0; j < 6; j++) {
                        System.out.print("   " + this.userBallsCollectionsForDrawings.get(i)[j] + "");
                    }
                    out(".\n    Kombinacijos statistika visuose tirazuose:");
                    if (l == 2) {
                        out("0   Tirazu, kuriuose nebuvo atspetas nei vienas kamuoliukas, skaicius: "
                                + this.singleCombinationStatistics.get(i).get("0") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("0"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                        out("0+  Tirazu, kuriuose buvo atspetas tik papildomas kamuoliukas: "
                                + this.singleCombinationStatistics.get(i).get("0+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("0+"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                        out("1   Tirazu, kuriuose buvo atspetas 1 pagrindinis kamuoliukas: "
                                + this.singleCombinationStatistics.get(i).get("1") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("1"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                        out("1+  Atspetas 1 pagrindinis ir papildomas kamuoliukas: "
                                + this.singleCombinationStatistics.get(i).get("1+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("1+"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                        out("2   Atspeti 2 pagrindiniai kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("2") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("2"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                        out("2+  Atspeti 2 pagrindiniai ir papildomas kamuoliukas: "
                                + this.singleCombinationStatistics.get(i).get("2+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("2+"), this.betsCollectionsForDrawings.size())
                                + "%) - nieko nelaimejo;");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("3") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("3") > 0) {
                            out2 = " - laimejo 1 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("3") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("3") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("3"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("3   Atspeti 3 kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("3") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("3"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("3+") > 0) || (l == 2)) {
                        if (this.singleCombination50centWon[i]) {
                            out3 = " 50 ct";
                            won3Plus += 0.5;
                        }
                        if (this.singleCombinationWonStatistics.get(i).get("3+") > 0) {
                            out2 = " - laimejo 3,50 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("3+") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("3+") + " Eur" + out3 + " ("
                                    + calculatePercent(won3Plus, thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("3+  Atspeti 3+ kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("3+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("3+"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("4") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("4") > 0) {
                            out2 = " - laimejo 11 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("4") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("4") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("4"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("4   Atspeti 4 kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("4") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("4"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("4+") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("4+") > 0) {
                            out2 = " - laimejo 68 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("4+") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("4+") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("4+"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("4+  Atspeti 4+ kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("4+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("4+"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("5") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("5") > 0) {
                            out2 = " - laimejo 200 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("5") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("5") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("5"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("5   Atspeti 5 kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("5") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("5"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("5+") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("5+") > 0) {
                            out2 = " - laimejo 5000 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("5+") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("5+") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("5+"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("5+  Atspeti 5+ kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("5+") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("5+"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ";");
                    }
                    if ((this.singleCombinationWonStatistics.get(i).get("6") > 0) || (l == 2)) {
                        if (this.singleCombinationWonStatistics.get(i).get("6") > 0) {
                            out2 = " - laimejo 100000 x " + out1 + " x "
                                    + this.singleCombinationStatistics.get(i).get("6") + " = "
                                    + this.singleCombinationWonStatistics.get(i).get("6") + " Eur ("
                                    + calculatePercent(this.singleCombinationWonStatistics.get(i).get("6"), thisCombinationWonTotal) + "%)";
                        } else {
                            out2 = " - nieko nelaimejo";
                        }
                        out("6   Atspeti 6 kamuoliukai: "
                                + this.singleCombinationStatistics.get(i).get("6") + " ("
                                + calculatePercent(this.singleCombinationStatistics.get(i).get("6"), this.betsCollectionsForDrawings.size())
                                + "%)" + out2 + ".");
                    }
                    if (this.singleCombinationWonTotal.get(i) > 0) {
                        out("    Bendra kombinacijos laimeta suma visuose tirazuose: "
                                + this.singleCombinationWonTotal.get(i) + " Eur" + out3
                                + " (" + calculatePercent(thisCombinationWonTotal, thisAmountWonTotal) + "%).");
                    } else {
                        out("    Kombinacija nieko nelaimejo.");
                    }
                }
            }
            out("\nBendra laimeta suma: " + this.amountWonTotal + " Eur" + _50centWon);
            if (l != 2) {
                out("\n");
                int choice = 0;
                do {
                    choice = inInt("Jums buvo atvaizduota kombinaciju statistika tik su laimingomis kombinacijosmis."
                            + "\n1. Jei norite perziureti pilna kombinaciju statistika, spauskite \"1\";\n2. Jei norite"
                            + " testi, spauskite \"2\", ir paspauskite Enter:");
                    if ((choice != 1) && (choice != 2)) {
                        out("\nIvedete ne ta skaiciu.");
                    }
                } while ((choice != 1) && (choice != 2));
                if (choice == 1) {
                    l = 2;
                }
            }
        }

    }
}
