/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static lt.vcs.MyUtils.*;

/**
 *
 * @author n.afanasenka
 */
public class Drawing {

    private int[] ballsCombination;
    private int ballAdditional;
    private int drawingNumber;
    private int[] guessedBalls;
    private boolean[] guessedAdditional;
    private int[] amountWonCollection;
    private boolean[] _50centCollection;
    private int amountWonTotal;
    private boolean _50cent;
    private Map<String, Integer> combinationStatistics;
    private Map<String, Integer> wonStatistics;
    private List<Map<String, Integer>> singleCombinationStatistics;
    private List<Map<String, Integer>> singleCombinationWonStatistics;

    public Drawing(int drawingNumber, List<Integer> betsCollection,
            List<int[]> userBallsCollectionInput) {

        this.drawingNumber = drawingNumber;
        this.ballsCombination = randomSix(1, 30);
        this.ballAdditional = random(1, 30);
        this.guessedBalls = new int[userBallsCollectionInput.size()];
        this.guessedAdditional = new boolean[userBallsCollectionInput.size()];
        this.amountWonCollection = new int[userBallsCollectionInput.size()];
        this._50centCollection = new boolean[userBallsCollectionInput.size()];
        this.amountWonTotal = 0;
        this._50cent = false;
        this.singleCombinationStatistics = new ArrayList<>();
        this.singleCombinationWonStatistics = new ArrayList<>();
        this.combinationStatistics = new HashMap<String, Integer>() {
            {
                put("0", 0);
                put("0+", 0);
                put("1", 0);
                put("1+", 0);
                put("2", 0);
                put("2+", 0);
                put("3", 0);
                put("3+", 0);
                put("4", 0);
                put("4+", 0);
                put("5", 0);
                put("5+", 0);
                put("6", 0);
            }
        };
        this.wonStatistics = new HashMap<String, Integer>() {
            {
                put("3", 0);
                put("3+", 0);
                put("4", 0);
                put("4+", 0);
                put("5", 0);
                put("5+", 0);
                put("6", 0);
            }
        };
        boolean error = true;
        while (error) {
            for (int i = 0; i < 6; i++) {
                if (this.ballsCombination[i] == this.ballAdditional) {
                    this.ballAdditional = random(1, 30);
                    error = true;
                    break;
                } else {
                    error = false;
                }
            }
        }

        for (int j = 0; j < userBallsCollectionInput.size(); j++) {
            this._50centCollection[j] = false;
            int[] usersBallsCombination = userBallsCollectionInput.get(j);
            this.guessedBalls[j] = 0;
            this.guessedAdditional[j] = false;
            Map<String, Integer> singleCombinationMap = new HashMap<String, Integer>() {
                {
                    put("0", 0);
                    put("0+", 0);
                    put("1", 0);
                    put("1+", 0);
                    put("2", 0);
                    put("2+", 0);
                    put("3", 0);
                    put("3+", 0);
                    put("4", 0);
                    put("4+", 0);
                    put("5", 0);
                    put("5+", 0);
                    put("6", 0);
                }
            };
            Map<String, Integer> singleCombinationWonMap = new HashMap<String, Integer>() {
                {
                    put("3", 0);
                    put("3+", 0);
                    put("4", 0);
                    put("4+", 0);
                    put("5", 0);
                    put("5+", 0);
                    put("6", 0);
                }
            };
            for (int k = 0; k < 6; k++) {
                if (usersBallsCombination[k] == this.ballAdditional) {
                    this.guessedAdditional[j] = true;
                    continue;
                }
                for (int l = 0; l < 6; l++) {
                    if (usersBallsCombination[k] == this.ballsCombination[l]) {
                        this.guessedBalls[j]++;
                        break;
                    }
                }
            }

            switch (this.guessedBalls[j]) {
                case 6:
                    this.amountWonCollection[j] = 100000 * betsCollection.get(j);
                    this.combinationStatistics.put("6", (this.combinationStatistics.get("6") + 1));
                    this.wonStatistics.put("6", (this.wonStatistics.get("6") + this.amountWonCollection[j]));
                    singleCombinationMap.put("6", 1);
                    singleCombinationWonMap.put("6", this.amountWonCollection[j]);
                    break;
                case 5:
                    if (this.guessedAdditional[j] == true) {
                        this.amountWonCollection[j] = 5000 * betsCollection.get(j);
                        this.combinationStatistics.put("5+", (this.combinationStatistics.get("5+") + 1));
                        this.wonStatistics.put("5+", (this.wonStatistics.get("5+") + this.amountWonCollection[j]));
                        singleCombinationMap.put("5+", 1);
                        singleCombinationWonMap.put("5+", this.amountWonCollection[j]);
                    } else {
                        this.amountWonCollection[j] = 200 * betsCollection.get(j);
                        this.combinationStatistics.put("5", (this.combinationStatistics.get("5") + 1));
                        this.wonStatistics.put("5", (this.wonStatistics.get("5") + this.amountWonCollection[j]));
                        singleCombinationMap.put("5", 1);
                        singleCombinationWonMap.put("5", this.amountWonCollection[j]);
                    }
                    break;
                case 4:
                    if (this.guessedAdditional[j] == true) {
                        this.amountWonCollection[j] = 68 * betsCollection.get(j);
                        this.combinationStatistics.put("4+", (this.combinationStatistics.get("4+") + 1));
                        this.wonStatistics.put("4+", (this.wonStatistics.get("4+") + this.amountWonCollection[j]));
                        singleCombinationMap.put("4+", 1);
                        singleCombinationWonMap.put("4+", this.amountWonCollection[j]);
                    } else {
                        this.amountWonCollection[j] = 11 * betsCollection.get(j);
                        this.combinationStatistics.put("4", (this.combinationStatistics.get("4") + 1));
                        this.wonStatistics.put("4", (this.wonStatistics.get("4") + this.amountWonCollection[j]));
                        singleCombinationMap.put("4", 1);
                        singleCombinationWonMap.put("4", this.amountWonCollection[j]);
                    }
                    break;
                case 3:
                    if (guessedAdditional[j] == true) {
                        if (betsCollection.get(j) % 2 != 0) {
                            this.amountWonCollection[j] = (3 * betsCollection.get(j))
                                    + (betsCollection.get(j) / 2);
                            this._50centCollection[j] = true;
                        } else {
                            this.amountWonCollection[j] = (3 * betsCollection.get(j))
                                    + (betsCollection.get(j) / 2);
                        }
                        this.combinationStatistics.put("3+", (this.combinationStatistics.get("3+") + 1));
                        this.wonStatistics.put("3+", (this.wonStatistics.get("3+") + this.amountWonCollection[j]));
                        singleCombinationMap.put("3+", 1);
                        singleCombinationWonMap.put("3+", this.amountWonCollection[j]);
                    } else {
                        this.amountWonCollection[j] = 1 * betsCollection.get(j);
                        this.combinationStatistics.put("3", (this.combinationStatistics.get("3") + 1));
                        this.wonStatistics.put("3", (this.wonStatistics.get("3") + this.amountWonCollection[j]));
                        singleCombinationMap.put("3", 1);
                        singleCombinationWonMap.put("3", this.amountWonCollection[j]);
                    }
                    break;
                case 2:
                    if (this.guessedAdditional[j] == true) {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("2+", (this.combinationStatistics.get("2+") + 1));
                        singleCombinationMap.put("2+", 1);
                    } else {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("2", (this.combinationStatistics.get("2") + 1));
                        singleCombinationMap.put("2", 1);
                    }
                    break;
                case 1:
                    if (this.guessedAdditional[j] == true) {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("1+", (this.combinationStatistics.get("1+") + 1));
                        singleCombinationMap.put("1+", 1);
                    } else {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("1", (this.combinationStatistics.get("1") + 1));
                        singleCombinationMap.put("1", 1);
                    }
                    break;
                case 0:
                    if (this.guessedAdditional[j] == true) {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("0+", (this.combinationStatistics.get("0+") + 1));
                        singleCombinationMap.put("0+", 1);
                    } else {
                        this.amountWonCollection[j] = 0;
                        this.combinationStatistics.put("0", (this.combinationStatistics.get("0") + 1));
                        singleCombinationMap.put("0", 1);
                    }
                    break;
            }

            this.singleCombinationStatistics.add(singleCombinationMap);
            this.singleCombinationWonStatistics.add(singleCombinationWonMap);
            this.amountWonTotal += this.amountWonCollection[j];
            if (this._50centCollection[j] == true) {
                if (this._50cent == true) {
                    this._50cent = false;
                    this.amountWonTotal++;
                    this.wonStatistics.put("3+", (this.wonStatistics.get("3+") + 1));
                } else {
                    this._50cent = true;
                }
            }
        }
    }

    public int[] getBallsCombiantion() {
        return ballsCombination;
    }

    public int getBallAdditional() {
        return ballAdditional;
    }

    public int getDrawingNumber() {
        return drawingNumber;
    }

    public int[] getGuessedBalls() {
        return guessedBalls;
    }

    public boolean[] getGuessedAdditional() {
        return guessedAdditional;
    }

    public int[] getAmountWonCollection() {
        return amountWonCollection;
    }

    public boolean[] get50centCollection() {
        return _50centCollection;
    }

    public int getAmountWonTotal() {
        return amountWonTotal;
    }

    public boolean get_50cent() {
        return _50cent;
    }

    public Map<String, Integer> getCombinationStatistics() {
        return combinationStatistics;
    }

    public Map<String, Integer> getWonStatistics() {
        return wonStatistics;
    }

    public List<Map<String, Integer>> getSingleCombinationStatistics() {
        return singleCombinationStatistics;
    }

    public List<Map<String, Integer>> getSingleCombinationWonStatistics() {
        return singleCombinationWonStatistics;
    }

}
