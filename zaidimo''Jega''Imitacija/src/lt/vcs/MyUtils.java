/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author n.afanasenka
 */
public class MyUtils {

    public static void out(Object txt) {
        System.out.println(txt);

    }

    private static Scanner newScan() {
        return new Scanner(System.in);
    }

    public static int inInt(String Query) {
        int number = 0;
        boolean error = true;
        while (error) {
            out(Query);
            String sNumber = newScan().nextLine();
            exit(sNumber);
            try {
                number = Integer.parseInt(sNumber.replace(" ", ""));
                error = false;
            } catch (Exception e) {
                if (sNumber.equals("")) {
                    out("");
                    out("Jus nieko neivedete.");
                } else {
                    out("");
                    out("Jus ivedete \"" + sNumber + "\" vietoj sveikojo skaiciaus.");
                }
            }
        }
        return number;
    }

    public static int dayInYear() {
        SimpleDateFormat sdf = new SimpleDateFormat("DDD");
        return Integer.parseInt(sdf.format(new Date()));
    }

    public static String inLine() {
        String sentence = newScan().nextLine();
        exit(sentence);
        return sentence;
    }

    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }

    public static int[] randomSix(int from, int to) {
        int[] combinationSix = new int[]{random(from, to), random(from, to),
            random(from, to), random(from, to), random(from, to),
            random(from, to)};
        for (int i = 0; i < 6; i++) {
            boolean error = true;
            while (error) {
                if ((combinationSix[i] != combinationSix[(i + 1) % 6])
                        && (combinationSix[i] != combinationSix[(i + 2) % 6])
                        && (combinationSix[i] != combinationSix[(i + 3) % 6])
                        && (combinationSix[i] != combinationSix[(i + 4) % 6])
                        && (combinationSix[i] != combinationSix[(i + 5) % 6])) {
                    error = false;
                } else {
                    combinationSix[i] = random(1, 30);
                }
            }
        }
        Arrays.sort(combinationSix);
        return combinationSix;
    }

    public static String calculatePercent(double valueToCalculate,
            double _100PercentValue) {
        return new DecimalFormat("#.##").format((valueToCalculate * 100)
                / _100PercentValue).replace(".", ",");
    }

    public static void exit(String threeZeros) {
        if (threeZeros.equals("000")) {
            out("");
            out("\nProgramos autorius Nikolajus linki jums grazios dienos! :) ");
            out("Paspauskite Enter, kad iseiti.");
            inLine();
            System.exit(0);
        }
    }
}
